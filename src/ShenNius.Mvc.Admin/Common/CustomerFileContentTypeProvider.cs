﻿using Microsoft.AspNetCore.StaticFiles;
using System;
using System.Collections.Generic;

namespace ShenNius.Mvc.Admin.Common
{
    public class CustomerFileContentTypeProvider : FileExtensionContentTypeProvider
    {
        public CustomerFileContentTypeProvider() :
            base(new Dictionary<string, string>(
                StringComparer.OrdinalIgnoreCase)
            {
                {
                    ".zip",
                    "application/x-zip-compressed"
                },
                {
                    ".less",
                    "stylesheet/css"
                },
                { ".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" },
            }
            )
        {

        }
    }
}

