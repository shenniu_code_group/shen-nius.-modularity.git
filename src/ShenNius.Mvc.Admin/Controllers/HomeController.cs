﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain;
using ShenNius.Domain.Entity.Cms;
using ShenNius.Domain.Entity.Shop;
using ShenNius.Domain.Repository.Sys;
using ShenNius.Domain.ValueObjects;
using SqlSugar;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISqlSugarClient _db;
        private readonly IMenuRepository _menuRepository;
        private readonly IShenNiusContext _userContext;
        public HomeController(ISqlSugarClient db, IMenuRepository menuRepository, IShenNiusContext userContext)
        {
            _db = db;
            _menuRepository = menuRepository;
            _userContext = userContext;
        }
        [HttpGet("shennius-master.html")]
        //必须设置下面该路由，不然报错404
        [Route("/")]
        public async Task<IActionResult> Index()
        {
            if (_userContext.IsAuthenticated())
            {
                //如果系统已经登录了，判断下即将请求用到的系统权限是否存在缓存，如果缓存为空，则去请求加热。               
                await _menuRepository.GetCurrentAuthMenus(_userContext.Id);
            }
            var value = await SysSetting.ReadAsync();
            return View(value);
        }
        public async Task<IActionResult> Report()
        {
            var articleCount = await _db.Queryable<Article>().Where(d => !d.IsDeleted).CountAsync();
            var columnCount = await _db.Queryable<Column>().Where(d => !d.IsDeleted).CountAsync();
            var goodsCount = await _db.Queryable<Goods>().Where(d => !d.IsDeleted).CountAsync();
            var categoryCount = await _db.Queryable<Category>().Where(d => !d.IsDeleted).CountAsync();
            var orderCount = await _db.Queryable<Order>().Where(d => !d.IsDeleted).CountAsync();
            ViewBag.articleCount = articleCount;
            ViewBag.columnCount = columnCount;
            ViewBag.goodsCount = goodsCount;
            ViewBag.categoryCount = categoryCount;
            ViewBag.orderCount = orderCount;
            return View();
        }
        [HttpGet("error.html")]
        public IActionResult Error()
        {
            return View();
        }
    }
}
