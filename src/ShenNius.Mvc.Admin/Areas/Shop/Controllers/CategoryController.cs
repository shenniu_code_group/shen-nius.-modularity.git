﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Shop;
using ShenNius.Infrastructure.Attributes;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Shop.Controllers
{
    [Area("shop")]
    public class CategoryController : Controller
    {
        private readonly IBaseRepository<Category> _categoryService;

        public CategoryController(IBaseRepository<Category> categoryService)
        {
            _categoryService = categoryService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet, MultiTenant]
        public async Task<IActionResult> Modify([FromQuery] DetailTenantQuery query)
        {
            Category model = query.Id == 0 ? new Category() : await _categoryService.GetModelAsync(d => d.Id == query.Id && !d.IsDeleted && d.TenantId == query.TenantId);
            return View(model);
        }
    }
}
