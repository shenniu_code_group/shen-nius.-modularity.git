﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Domain.Repository.Shop;
using ShenNius.Infrastructure.Attributes;
using ShenNius.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Shop.Controllers
{
    [Area("shop")]
    public class GoodsController : Controller
    {
        private readonly IGoodsRepository _goodsService;
        private readonly IBaseRepository<Config> _configService;
        public GoodsController(IGoodsRepository goodsService, IBaseRepository<Config> configService)
        {
            _goodsService = goodsService;
            _configService = configService;

        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            var datas = await GetConfigs();
            return View();
        }
        [HttpGet, MultiTenant]
        public async Task<IActionResult> Modify([FromQuery] DetailTenantQuery query)
        {
            var result = await _goodsService.DetailAsync(query);
            var datas = await GetConfigs();
            ViewBag.Freights = datas;

            return View(result.Data);
        }
        private async Task<List<Config>> GetConfigs() => await _configService.GetListAsync(d => d.Type.Equals("Freight") && !d.IsDeleted);

    }
}
