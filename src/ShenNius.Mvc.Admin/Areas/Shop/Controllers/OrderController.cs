﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Repository.Shop;
using ShenNius.Infrastructure.Attributes;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Shop.Controllers
{
    [Area("shop")]
    public class OrderController : Controller
    {
        private readonly IOrderRepository _orderRepository;

        public OrderController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet, MultiTenant]
        public async Task<IActionResult> Detail([FromQuery] DetailTenantQuery query)
        {
            var model = await _orderRepository.GetOrderDetailAsync(query);
            if (model == null)
            {
                return NotFound();
            }
            return View(model.Data);
        }
    }
}
