﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using ShenNius.Caches;
using ShenNius.Common;
using ShenNius.Common.Extension;
using ShenNius.Common.Hepler;
using ShenNius.Domain;
using ShenNius.Domain.Dtos.Input.Sys;
using ShenNius.Domain.Dtos.Output.Sys;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Domain.Repository.Sys;
using ShenNius.Domain.ValueObjects;
using ShenNius.Domain.ValueObjects.Enums.Sys;
using ShenNius.Infrastructure.Hubs;
using ShenNius.Mvc.Admin.Common;
using ShenNius.Repository;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Sys.Controllers
{
    [Area("sys")]
    public partial class UserController : Controller
    {
        private readonly IDistributedCache _cacheHelper;
        private readonly IShenNiusContext _sc;
        private readonly IBaseRepository<User> _userRepository;
        private readonly IMenuRepository _menuRepository;
        private readonly IMapper _mapper;
        private readonly UserLoginNotifiHub _userLoginNotifiHub;
        private readonly IBaseRepository<Config> _configRepository;
        public UserController(IDistributedCache cacheHelper, IShenNiusContext currentUserContext, IBaseRepository<User> userRepository, IMenuRepository menuRepository, IMapper mapper, UserLoginNotifiHub userLoginNotifiHub, IBaseRepository<Config> configRepository)
        {
            _configRepository = configRepository;
            _cacheHelper = cacheHelper;
            _sc = currentUserContext;
            _userRepository = userRepository;
            _menuRepository = menuRepository;
            _mapper = mapper;
            _userLoginNotifiHub = userLoginNotifiHub;
        }
        /// <summary>
        /// 用户首页列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 设置角色
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult SetRole(int id)
        {
            ViewBag.UserId = id;
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            User model = null;
            if (id == 0)
            {
                model = new User();
            }
            else
            {
                /*不需要赋值查询租户，对超级管理员来说需要看到所有的用户信息；对当前登录的用户所从属的租户来说，没必要再赋值 _sc.TenantId查询条件；如果选择谨慎一点，需要加上查询条件按，则需要对当前登录的用户判断属于什么系统级别，分别赋予不同的条件按，比较繁琐*/
                model = await _userRepository.GetModelAsync(d => d.Id == id && !d.IsDeleted);
            }
            ViewBag.AllTenans = _sc.GetAllTenans();
            return View(model);
        }
        [HttpGet]
        public IActionResult ModifyPwd()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CurrentUserInfo()
        {
            if (!_sc.IsAuthenticated())
            {
                Redirect("/user/login");
            }
            var user = _sc.GetCurrentUserInfo();
            return View(user);
        }
        [HttpGet, AllowAnonymous]
        public async Task<IActionResult> Login()
        {
            var rsaKey = RSACrypt.GetKey();
            var number = Guid.NewGuid().ToString();
            if (rsaKey.Count <= 0 || rsaKey == null)
            {
                throw new ArgumentNullException("获取登录的公钥和私钥为空");
            }
            ViewBag.RsaKey = rsaKey[0];
            ViewBag.Number = number;
            //获得公钥和私钥
            _cacheHelper.Set($"{SysCacheKey.EncryLoginKey}:{number}", rsaKey);
            var value = await SysSetting.ReadAsync();

            return View(value);
        }

        [HttpPost, AllowAnonymous]
        public async Task<ApiResult<LoginOutput>> Login([FromBody] LoginInput input)
        {
            try
            {
                var rsaKey = _cacheHelper.Get<List<string>>($"{SysCacheKey.EncryLoginKey}:{input?.NumberGuid}");
                if (rsaKey == null)
                {
                    throw new ArgumentException("登录失败，请刷新浏览器再次登录!");
                }
                var ras = new RSACrypt(rsaKey[0], rsaKey[1]);
                input.Password = ras.Decrypt(input.Password);

                input.Password = Md5Crypt.Encrypt(input.Password);
                var loginModel = await _userRepository.GetModelAsync(d => d.Name.Equals(input.LoginName) && d.Password.Equals(input.Password));
                if (loginModel?.Id == 0)
                {
                    _sc.WriteLog($"系统用户{input.LoginName}登陆失败，用户名或密码错误！账号信息为:{input.LoginName}/{input.Password}", loginModel.Id);
                    return new ApiResult<LoginOutput>("用户名或密码错误", 500);
                }
                if (!input.ConfirmLogin)
                {
                    if (loginModel.IsLogin)
                    {
                        return new ApiResult<LoginOutput>($"该用户【{input.LoginName}】已经登录，此时强行登录，其他地方会被挤下线！", 200);
                    }
                }
                loginModel.ModifyLoginInfo();
                await _userRepository.UpdateAsync(loginModel);
                var result = _mapper.Map<LoginOutput>(loginModel);

                //请求当前用户的所有权限并存到缓存里面并发给前端 准备后面鉴权使用
                var menuAuths = await _menuRepository.GetCurrentAuthMenus(result.Id);
                if (menuAuths == null || menuAuths.Count == 0)
                {
                    return new ApiResult<LoginOutput>("不好意思，该用户当前没有权限。请联系系统管理员分配权限！");
                }
                result.MenuAuthOutputs = menuAuths;
                if (input.ConfirmLogin)
                {
                    await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                    _cacheHelper.Remove($"{SysCacheKey.AuthMenu}:{_sc.Id}");
                }
                if (loginModel.TenantId == 0)
                {
                    return new ApiResult<LoginOutput>("不好意思,您当前的账号没有分配租户,请联系管理员分配租户！");
                }
                int isAdmin = 0;
                /*判断系统登录用户是什么角色*/
                var roleModel = await _sc.Db.Queryable<Role, R_User_Role>((a, c) => new JoinQueryInfos(JoinType.Inner, a.Id == c.RoleId && !a.IsDeleted && !c.IsDeleted)).Where((a, c) => c.UserId == loginModel.Id).Select((a, c) => new RoleOutput { Name = a.Name, Id = a.Id }).FirstAsync();
                if (roleModel != null)
                {
                    var roleText = RoleEnum.SuperAdmin.GetEnumText();
                    if (roleModel.Name.Equals(roleText))
                    {
                        isAdmin = 1;
                    }
                }

                var identity = new ClaimsPrincipal(
                   new ClaimsIdentity(new[]
                       {
                              new Claim(JwtRegisteredClaimNames.Sid,result.Id.ToString()),
                              new Claim(ClaimTypes.Name,result.LoginName),
                              new Claim(ClaimTypes.WindowsAccountName,result.LoginName),
                              new Claim(ClaimTypes.UserData,result.LastLoginTime.ToString()),
                              new Claim(ClaimTypes.MobilePhone,result.Mobile),
                              new Claim(ClaimTypes.Email,loginModel.Email),
                              new Claim("TrueName",result.TrueName),
                              new Claim("TenantId",result.TenantId.ToString()),
                               new Claim("IsAdmin",isAdmin.ToString())
                       }, CookieAuthenticationDefaults.AuthenticationScheme)
                  );
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, identity, new AuthenticationProperties
                {
                    ExpiresUtc = DateTime.UtcNow.AddHours(24),
                    IsPersistent = true,
                    AllowRefresh = false
                });
                _cacheHelper.Remove($"{SysCacheKey.EncryLoginKey}:{input.NumberGuid}");
                _sc.WriteLog($"系统用户{result.LoginName}登陆成功", result.Id, null, result.TenantId);
                return new ApiResult<LoginOutput>(result);
            }
            catch (Exception ex)
            {
                ApiResult<LoginOutput> result = new ApiResult<LoginOutput>(msg: $"登陆失败，请刷新浏览器重新尝试登录！{ex.Message}");
                _sc.WriteLog($"系统用户{input.LoginName}登陆失败:{ex.Message}");
                return result;
            }
        }


        [HttpGet, AllowAnonymous]
        public FileResult OnGetVCode()
        {
            var vcode = VerifyCode.CreateRandomCode(4);
            HttpContext.Session.SetString("vcode", vcode);
            var img = VerifyCode.DrawImage(vcode, 20, Color.White);
            return File(img, "image/gif");
        }

        [HttpGet]
        public async Task<ApiResult> Logout()
        {
            try
            {
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                _cacheHelper.Remove($"{SysCacheKey.AuthMenu}:{_sc.Id}");
                _sc.WriteLog($"系统用户{_sc.Name}成功退出系统!");
                //设置用户退出
                await _userRepository.UpdateAsync(d => new User() { IsLogin = false }, d => d.Id == _sc.Id);
                _userLoginNotifiHub.Clear();
                return new ApiResult();
            }
            catch
            {
                return new ApiResult("退出失败了！");
            }
        }

    }
}
