﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Sys.Controllers
{
    [Area("sys")]
    public class LogsController : Controller
    {
        private readonly IBaseRepository<Log> _logService;
        public LogsController(IBaseRepository<Log> logService)
        {
            _logService = logService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Detail(int id)
        {
            var model = await _logService.GetModelAsync(d => d.Id == id);
            return View(model);
        }
        [HttpGet]
        public IActionResult Echarts()
        {
            return View();
        }
    }
}
