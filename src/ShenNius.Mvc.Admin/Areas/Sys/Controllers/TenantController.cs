﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Sys.Controllers
{
    [Area("sys")]
    public class TenantController : Controller
    {
        private readonly IBaseRepository<Tenant> _tenantRepository;

        public TenantController(IBaseRepository<Tenant> tenantRepository)
        {
            _tenantRepository = tenantRepository;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        //
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            Tenant tenant = null;
            if (id == 0)
            {
                tenant = new Tenant();
            }
            else
            {
                tenant = await _tenantRepository.GetModelAsync(d => d.Id == id && !d.IsDeleted);
            }
            return View(tenant);
        }
    }
}
