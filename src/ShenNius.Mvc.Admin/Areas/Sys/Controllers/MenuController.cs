﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Dtos.Output.Sys;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Domain.Repository.Sys;
using ShenNius.Domain.ValueObjects.Enum;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Sys.Controllers
{
    [Area("sys")]
    public class MenuController : Controller
    {
        private readonly IMenuRepository _menuRepository;
        private readonly IBaseRepository<Config> _configService;
        public MenuController(IMenuRepository menuRepository, IBaseRepository<Config> configService)
        {
            _menuRepository = menuRepository;
            _configService = configService;

        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify([FromQuery] DetailQuery query)
        {
            MenuDetailOutput model = new MenuDetailOutput();
            string alreadyBtns = string.Empty;
            if (query.Id > 0)
            {
                model.MenuOutput = await _menuRepository.GetModelAsync(d => d.Id == query.Id && !d.IsDeleted);
                if (model.MenuOutput != null)
                {
                    if (model.MenuOutput.BtnCodeIds.Length > 0)
                    {
                        for (int i = 0; i < model.MenuOutput.BtnCodeIds.Length; i++)
                        {
                            alreadyBtns += model.MenuOutput.BtnCodeIds[i] + ",";
                        }
                        if (!string.IsNullOrEmpty(alreadyBtns))
                        {
                            alreadyBtns = alreadyBtns.TrimEnd(',');
                        }
                    }
                }
                ViewBag.AlreadyBtns = alreadyBtns;
            }
            else
            {
                model.MenuOutput = new Menu();
            }
            var configs = await _configService.GetListAsync(d => d.Type == nameof(ConfigEnum.Button) && !d.IsDeleted);
            model.ConfigOutputs = configs;
            return View(model);
        }
    }
}
