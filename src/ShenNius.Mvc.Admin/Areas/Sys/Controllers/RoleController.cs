﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Sys.Controllers
{
    [Area("sys")]
    public class RoleController : Controller
    {
        private readonly IBaseRepository<Role> _roleService;
        public RoleController(IBaseRepository<Role> roleService)
        {
            _roleService = roleService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Modify(int id)
        {
            Role model = id == 0 ? new Role() : await _roleService.GetModelAsync(d => d.Id == id && !d.IsDeleted);
            return View(model);
        }
        [HttpGet]
        public IActionResult SetMenu(int id)
        {
            ViewBag.RoleId = id;
            return View();
        }
    }
}
