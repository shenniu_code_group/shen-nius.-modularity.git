﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain;
using ShenNius.Domain.Entity.Cms;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Cms.Controllers
{
    [Area("cms")]
    public class ArticleController : Controller
    {
        private readonly IBaseRepository<Article> _articleService;
        private readonly IShenNiusContext _sc;

        public ArticleController(IBaseRepository<Article> articleService, IShenNiusContext sc)
        {
            _articleService = articleService;
            _sc = sc;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            var model = id == 0 ? new Article() : await _articleService.GetModelAsync(d => d.Id == id && !d.IsDeleted && _sc.TenantId == d.TenantId);
            return View(model);
        }
    }
}
