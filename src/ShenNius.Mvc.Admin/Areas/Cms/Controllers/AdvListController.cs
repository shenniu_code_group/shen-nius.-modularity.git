﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Cms.API.Domain.ValueObjects.Enums;
using ShenNius.Common.Extension;
using ShenNius.Domain;
using ShenNius.Domain.Entity.Cms;
using ShenNius.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Cms.Controllers
{
    [Area("cms")]
    public class AdvListController : Controller
    {
        private readonly IBaseRepository<AdvList> _advListService;
        private readonly IShenNiusContext _sc;

        public AdvListController(IBaseRepository<AdvList> advListService, IShenNiusContext sc)
        {
            _advListService = advListService;
            _sc = sc;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> Modify(int id = 0)
        {
            AdvList model = id == 0 ? new AdvList() : await _advListService.GetModelAsync(d => d.Id == id && !d.IsDeleted && d.TenantId == _sc.TenantId);

            Dictionary<int, string> dic = new Dictionary<int, string>
            {
                { AdvEnum.FriendlyLink.GetValue<int>(), AdvEnum.FriendlyLink.GetEnumText() },
                 { AdvEnum.Slideshow.GetValue<int>(), AdvEnum.Slideshow.GetEnumText() },
                  { AdvEnum.GoodBlog.GetValue<int>(), AdvEnum.GoodBlog.GetEnumText() },
                   { AdvEnum.MiniApp.GetValue<int>(), AdvEnum.MiniApp.GetEnumText() },
            };
            ViewBag.Dic = dic;
            return View(model);
        }
    }
}
