﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Cms;
using ShenNius.Infrastructure.Attributes;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Cms.Controllers
{
    [Area("cms")]
    public partial class KeywordController : Controller
    {
        private readonly IBaseRepository<Keyword> _keywordService;

        public KeywordController(IBaseRepository<Keyword> KeywordService)
        {
            _keywordService = KeywordService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet, MultiTenant]
        public async Task<IActionResult> Modify([FromQuery] DetailTenantQuery query)
        {
            Keyword model = query.Id == 0 ? new Keyword() : await _keywordService.GetModelAsync(d => d.Id == query.Id && !d.IsDeleted && d.TenantId == query.TenantId);
            return View(model);
        }
        [HttpGet]
        public IActionResult ImportKey()
        {
            return View();
        }

    }
}
