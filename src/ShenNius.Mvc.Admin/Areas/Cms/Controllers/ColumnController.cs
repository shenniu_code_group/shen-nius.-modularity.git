﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Cms;
using ShenNius.Infrastructure.Attributes;
using ShenNius.Repository;
using System.Threading.Tasks;

namespace ShenNius.Mvc.Admin.Areas.Cms.Controllers
{
    [Area("cms")]
    public class ColumnController : Controller
    {
        private readonly IBaseRepository<Column> _columnService;

        public ColumnController(IBaseRepository<Column> columnService)
        {
            _columnService = columnService;
        }
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet, MultiTenant]
        public async Task<IActionResult> Modify([FromQuery] DetailTenantQuery query)
        {
            Column model = query.Id == 0 ? new Column() : await _columnService.GetModelAsync(d => d.Id == query.Id && !d.IsDeleted && d.TenantId == query.TenantId);
            return View(model);
        }
    }
}
