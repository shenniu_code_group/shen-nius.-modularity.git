using Microsoft.AspNetCore.Builder;
using ShenNius.Admin.Mvc;
using ShenNius.ModuleCore.Extensions;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddModule<ShenniusAdminMvcModule>(builder.Configuration);
var app = builder.Build();
app.UseModule();
app.Run();



