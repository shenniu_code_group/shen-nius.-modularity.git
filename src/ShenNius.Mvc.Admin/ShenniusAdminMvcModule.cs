﻿using Microsoft.AspNetCore.Builder;
using ShenNius.Admin.API;
using ShenNius.ModuleCore;
using ShenNius.ModuleCore.Context;
using ShenNius.ModuleCore.Extensions;
using ShenNius.Mvc.Admin.Common;

namespace ShenNius.Admin.Mvc
{
    [DependsOn(
         typeof(ShenniusAdminApiModule)       
         )]
    public class ShenniusAdminMvcModule : AppModule
    {
        public override void OnConfigureServices(ServiceConfigurationContext context)
        {
         
           
        }
        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();
            app.UseStaticFiles(new StaticFileOptions
            {
                ContentTypeProvider = new CustomerFileContentTypeProvider()
            });
        }
    }
}
