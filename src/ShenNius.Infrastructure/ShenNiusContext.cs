﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using ShenNius.Caches;
using ShenNius.Common.Hepler;
using ShenNius.Domain;
using ShenNius.Domain.Dtos.Output.Sys;
using ShenNius.Domain.Entity.Sys;
using SqlSugar;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
namespace ShenNius.Infrastructure
{
    /// <summary>
    /// 用户上下文
    /// </summary>
    public class ShenNiusContext : IShenNiusContext
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly IDistributedCache _cacheHelper;
        public ISqlSugarClient Db { get; }
        public ShenNiusContext(IHttpContextAccessor accessor, IDistributedCache cacheHelper)
        {
            _accessor = accessor;
            _cacheHelper = cacheHelper;
            Db = MyHttpContext.ServiceProvider.CreateScope().ServiceProvider.GetRequiredService<ISqlSugarClient>();
        }
        public int TenantId => GetTenants().FirstOrDefault(d => d.IsCurrent).Id;
        public string TenantName => GetTenants().FirstOrDefault(d => d.IsCurrent).Name;

        public List<TenantOutput> GetAllTenans() => GetTenants();


        private List<TenantOutput> GetTenants()
        {
            var tenants = _cacheHelper.Get<List<TenantOutput>>($"{SysCacheKey.AllTenant}:{Id}");
            if (tenants == null)
            {
                var res = Db.Queryable<Tenant>().Where(d => !d.IsDeleted).ToList();
                var data = new List<TenantOutput>();
                var userTenantId = Convert.ToInt32(GetClaimValueByType("TenantId").FirstOrDefault());
                if (userTenantId == 0)
                {
                    throw new ArgumentNullException("当前登录的用户查找不到关联的租户");
                }
                foreach (var item in res)
                {
                    if (userTenantId == item.Id)
                    {
                        data.Add(new TenantOutput() { Id = item.Id, Name = item.Name, IsCurrent = true });
                    }
                    else
                    {
                        data.Add(new TenantOutput() { Id = item.Id, Name = item.Name, IsCurrent = false });
                    }
                }
                if (data.Count > 0)
                {
                    _cacheHelper.Set($"{SysCacheKey.AllTenant}:{Id}", data);
                }
                return data;
            }
            return tenants;
        }

        public string Name => GetName();

        private string GetName()
        {
            if (IsAuthenticated())
            {
                return _accessor.HttpContext.User.Identity.Name;
            }
            else
            {
                if (!string.IsNullOrEmpty(GetToken()))
                {
                    return GetUserInfoFromToken("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name").FirstOrDefault().ToString();
                }
            }
            return "";
        }


        public int Id => Convert.ToInt32(GetClaimValueByType(JwtRegisteredClaimNames.Sid).FirstOrDefault());

        public string Ip => _accessor.HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault() ?? _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
        public string Address => IpParseHelper.GetAddressByIP(Ip);



        public bool IsAuthenticated()
        {
            if (_accessor.HttpContext?.User != null)
            {
                return _accessor.HttpContext.User.Identity.IsAuthenticated;
            }
            return false;
        }


        public string GetToken()
        {
            return _accessor.HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
        }

        public List<string> GetUserInfoFromToken(string claimType)
        {
            var jwtHandler = new JwtSecurityTokenHandler();
            if (!string.IsNullOrEmpty(GetToken()))
            {
                JwtSecurityToken jwtToken = jwtHandler.ReadJwtToken(GetToken());

                return (from item in jwtToken.Claims
                        where item.Type == claimType
                        select item.Value).ToList();
            }
            else
            {
                return new List<string>() { };
            }
        }

        public IEnumerable<Claim> GetClaimsIdentity()
        {
            return _accessor.HttpContext.User.Claims;
        }

        public List<string> GetClaimValueByType(string claimType)
        {
            return (from item in GetClaimsIdentity()
                    where item.Type == claimType
                    select item.Value).ToList();
        }

        public bool IsAdmin()
        {
            return GetClaimsIdentity().FirstOrDefault(x => x.Type.Equals("IsAdmin"))?.Value ==
                   "1";
        }
        public UserOutput GetCurrentUserInfo()
        {
            UserOutput userOutput = new UserOutput()
            {
                Name = Name,
                Id = Id,
            };
            userOutput.Mobile = GetClaimsIdentity().FirstOrDefault(d => d.Type == ClaimTypes.MobilePhone)?.Value;
            userOutput.Email = GetClaimsIdentity().FirstOrDefault(d => d.Type == ClaimTypes.Email)?.Value;
            userOutput.TrueName = GetClaimsIdentity().FirstOrDefault(d => d.Type == "TrueName")?.Value;
            userOutput.TenantName = TenantName;
            return userOutput;
        }
        public void WriteLog(string msg, int userId = 0, string application = null, int tenantId = 0)
        {
            var log = Log.Create(msg, userId, application, tenantId);
            Db.Insertable<Log>(log).ExecuteReturnIdentity();
        }
    }
}
