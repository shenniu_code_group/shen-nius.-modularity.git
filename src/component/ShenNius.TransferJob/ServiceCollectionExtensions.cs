﻿using Microsoft.Extensions.DependencyInjection;

namespace ShenNius.TransferJob
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddTransferJob(this IServiceCollection services)
        {
            services.AddSingleton<IBackgroundRunService, BackgroundRunService>();
            services.AddHostedService<TransferJobHostedService>();
            return services;
        }
    }
}
