# ShenNius.Caches

#### 介绍
ShenNius.Caches是基于Asp.NetCore对IDistributedCache接口扩展的补充，包括常见的泛型Get，Set，微软封装的只支持字节，字符串操作。该库是更高级别的抽象封装，主要目的是方便业务模块化快速开发使用。

#### 软件架构
整体围绕"高内聚，低耦合"的思想，依赖抽象而不依赖于具体。

#### 安装教程
1.  使用前先查看微软文档，方便快速了解https://docs.microsoft.com/zh-cn/aspnet/core/performance/caching/distributed?view=aspnetcore-6.0
1.  Nuget搜ShenNius.Caches选择安装
2.  在startup或program中进行注入    
     services.AddDistributedMemoryCache();    

#### 使用说明
1.  ShenNius.FileManagement目前只支持Asp.NetCore
2.  ShenNius.Caches目前分别引用了Microsoft.Extensions.Caching.Memory、      Microsoft.Extensions.Caching.StackExchangeRedis、Newtonsoft.Json
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

