﻿namespace ShenNius.FileManagement
{
    public enum FileType
    {
        /// <summary>
        /// 本地上传
        /// </summary>
        Local,
        /// <summary>
        /// 七牛云
        /// </summary>
        QiniuCloud
    }
}
