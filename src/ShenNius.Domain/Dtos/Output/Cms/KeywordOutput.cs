﻿namespace ShenNius.Domain.Dtos.Output.Cms
{
    public class KeywordOutput
    {
        public string Title { get; set; }
        public string Url { get; set; }
    }
}
