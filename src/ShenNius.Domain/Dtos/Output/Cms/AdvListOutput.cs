﻿using ShenNius.Cms.API.Domain.ValueObjects.Enums;
using ShenNius.Common.Extension;

namespace ShenNius.Domain.Dtos.Output.Cms
{
    public class AdvListOutput
    {
        public string Title { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime? ModifyTime { get; set; }
        public string TenantName { get; set; }
        public int Id { get; set; }
        public string Summary { get; set; }
        public AdvEnum Type { get; set; }

        public string TypeName
        {
            get
            {
                string name = "";
                if (Type == AdvEnum.FriendlyLink)
                {
                    name = AdvEnum.FriendlyLink.GetEnumText();
                }
                if (Type == AdvEnum.Slideshow)
                {
                    name = AdvEnum.Slideshow.GetEnumText();
                }
                if (Type == AdvEnum.GoodBlog)
                {
                    name = AdvEnum.GoodBlog.GetEnumText();
                }
                if (Type == AdvEnum.MiniApp)
                {
                    name = AdvEnum.MiniApp.GetEnumText();
                }
                return name;
            }
        }
    }
}
