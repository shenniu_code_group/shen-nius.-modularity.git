﻿namespace ShenNius.Domain.Dtos.Output.Sys
{
    public class ConfigBtnOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
    }
}
