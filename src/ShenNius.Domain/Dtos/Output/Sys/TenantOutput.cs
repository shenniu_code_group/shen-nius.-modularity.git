﻿namespace ShenNius.Domain.Dtos.Output.Sys
{
    public class TenantOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsCurrent { get; set; }
    }
}
