﻿namespace ShenNius.Domain.Dtos.Output.Sys
{
    public class ParentMenuOutput
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
