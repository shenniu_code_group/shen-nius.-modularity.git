﻿using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{
    public class ConfigInput
    {
        [Required(ErrorMessage = "名称必填")]
        public string Name { get; set; }
        [Required(ErrorMessage = "英文名称必填")]
        public string EnName { get; set; }
        [Required(ErrorMessage = "备注必填")]
        public string Remark { get; set; }
        [Required(ErrorMessage = "类型必须填写")]
        public string Type { get; set; }

    }
    public class ConfigModifyInput : ConfigInput
    {
        public int Id { get; set; }
    }
}
