﻿using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{
    public class SetRoleMenuInput
    {
        [Required(ErrorMessage = "角色id不能为空")]
        public int RoleId { get; set; }
        [Required(ErrorMessage = "菜单id至少填写一个")]
        public List<int> MenuIds { get; set; }
    }
}
