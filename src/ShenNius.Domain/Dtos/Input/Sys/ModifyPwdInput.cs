﻿using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{
    public class ModifyPwdInput
    {
        [Required(ErrorMessage = "修改密码的用户Id不能为空")]
        public int Id { get; set; }
        [Required(ErrorMessage = "新密码不能为空")]
        public string NewPassword { get; set; }
        [Required(ErrorMessage = "确认的新密码不能为空")]
        public string ConfirmPassword { get; set; }
        /// <summary>
        /// 旧密码
        /// </summary>
        [Required(ErrorMessage = "旧密码不能为空")]
        public string OldPassword { get; set; }

        public void CheckConfirmPassword()
        {
            if (!ConfirmPassword.Equals(NewPassword))
            {
                throw new ArgumentException("两次输入的密码不一致");
            }
        }
    }
}
