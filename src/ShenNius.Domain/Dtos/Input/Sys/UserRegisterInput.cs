﻿using ShenNius.Domain.Dtos.Common;
using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{
    public class UserRegisterInput : BaseTenant
    {
        /// <summary>
        /// 登录账号
        /// </summary>
        [Required(ErrorMessage = "登录账号必填")]
        public string Name { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        [Required(ErrorMessage = "登录密码必填")]
        public string Password { get; set; }
        /// <summary>
        /// 真是姓名
        /// </summary>
        [Required(ErrorMessage = "真实姓名必填")]
        public string TrueName { get; set; }
        public string Sex { get; set; } = "男";
        /// <summary>
        /// 手机号码
        /// </summary>
        [Required(ErrorMessage = "手机号码必填")]
        public string Mobile { get; set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        [Required(ErrorMessage = "邮箱必填")]
        public string Email { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [Required(ErrorMessage = "备注必填")]
        public string Remark { get; set; }

    }
    public class UserModifyInput : UserRegisterInput
    {
        [Required(ErrorMessage = "用户Id必须传递")]
        public int Id { get; set; }
    }
}
