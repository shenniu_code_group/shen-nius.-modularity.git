﻿using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{
    public class MenuInput
    {
        // [Required(ErrorMessage = "父菜单必选")]
        public int ParentId { get; set; }
        [Required(ErrorMessage = "菜单名称必填")]
        public string Name { get; set; }
        [Required(ErrorMessage = "菜单权限码必填")]
        public string NameCode { get; set; }
        // [Required(ErrorMessage = "菜单url必填")]  有些顶级菜单url不用填
        public string Url { get; set; }
        [Required(ErrorMessage = "HttpMethod必填")]
        public string HttpMethod { get; set; }
        public bool Status { get; set; }
        public int Sort { get; set; }
        [Required(ErrorMessage = "菜单图标必填")]
        public string Icon { get; set; }
        /// <summary>
        /// 按钮选择框
        /// </summary>
        public string[] BtnCodeIds { get; set; }
    }
    public class MenuModifyInput : MenuInput
    {
        [Required(ErrorMessage = "菜单Id不能为空")]
        public int Id { get; set; }
    }
}
