﻿using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{
    public class RoleMenuBtnInput
    {
        [Required(ErrorMessage = "RoleId不能为空")]
        public int RoleId { get; set; }
        [Required(ErrorMessage = "MenuId不能为空")]
        public int MenuId { get; set; }
        [Required(ErrorMessage = "菜单按钮Id必须传递")]
        public string BtnCodeId { get; set; }
        public bool Status { get; set; }
    }
}
