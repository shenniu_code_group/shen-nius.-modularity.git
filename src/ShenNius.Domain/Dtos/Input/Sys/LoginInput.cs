﻿using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{
    public class LoginInput
    {
        /// <summary>
        /// 登录账号
        /// </summary>
        [Required(ErrorMessage = "登录账号名称必填")]
        public string LoginName { get; set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        [Required(ErrorMessage = "请填写用户密码")]
        public string Password { get; set; }
        [Required(ErrorMessage = "用户编号必须传递")]
        public string NumberGuid { get; set; }
        /// <summary>
        /// 确定再次登录
        /// </summary>
        public bool ConfirmLogin { get; set; }
    }
}
