﻿/*************************************
* 类 名： TenantCurrentInput
* 作 者： realyrare
* 邮 箱： mahonggang8888@126.com
* 时 间： 2021/3/18 17:39:18
* .netV： 3.1
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{
    public class TenantCurrentInput
    {
        [Required(ErrorMessage = "Id不能为空")]
        public int Id { get; set; }
    }
}