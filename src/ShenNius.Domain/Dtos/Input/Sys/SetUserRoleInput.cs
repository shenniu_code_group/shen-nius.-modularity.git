﻿using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{
    public class SetUserRoleInput
    {
        [Required(ErrorMessage = "用户id必须填写")]
        public int UserId { get; set; }
        [Required(ErrorMessage = "角色id不能为空")]
        public int RoleId { get; set; }
        /// <summary>
        /// 只用于前台传值
        /// </summary>
        public bool Status { get; set; } = true;
    }
}
