﻿using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{
    public class PermissionsInput
    {
        [Display(Name = "角色Id")]
        [Required(ErrorMessage = "{0}是必填的")]
        public int RoleId { get; set; }
        [Display(Name = "菜单Id")]
        [Required(ErrorMessage = "{0}是必填的")]
        [Range(1, 100000)]
        public int MenuId { get; set; }
    }
}
