﻿using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Sys
{

    public class RoleInput
    {
        [Required(ErrorMessage = "角色名称必填")]
        public string Name { get; set; }
        [Required(ErrorMessage = "角色备注必填")]
        public string Description { get; set; }
    }
    public class RoleModifyInput : RoleInput
    {
        [Required(ErrorMessage = "角色Id不能为空")]
        public int Id { get; set; }
    }
}
