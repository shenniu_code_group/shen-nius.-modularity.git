﻿using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Common;
using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Cms
{
    public class ArticleInput : BaseTenant
    {

        /// <summary>
        /// Desc:栏目ID
        /// Default:0
        /// Nullable:False
        /// </summary>
        public int ColumnId { get; set; } = 0;
        /// <summary>
        /// Desc:文章标题
        /// Default:-
        /// Nullable:False
        /// </summary>
        [Required(ErrorMessage = "文章标题不能为空")]
        public string Title { get; set; }

        /// <summary>
        /// Desc:文章标题颜色
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string TitleColor { get; set; }

        /// <summary>
        /// Desc:文章副标题
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string SubTitle { get; set; }

        /// <summary>
        /// Desc:作者
        /// Default:-
        /// Nullable:True
        /// </summary>
        [Required(ErrorMessage = "文章作者不能为空")]
        public string Author { get; set; }

        /// <summary>
        /// Desc:来源
        /// Default:-
        /// Nullable:True
        /// </summary>
        [Required(ErrorMessage = "文章来源不能为空")]
        public string Source { get; set; }

        /// <summary>
        /// Desc:是否外链
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsLink { get; set; } = false;

        /// <summary>
        /// Desc:外部链接地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string LinkUrl { get; set; }

        /// <summary>
        /// Desc:文章标签
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Tag { get; set; }

        /// <summary>
        /// Desc:文章图
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string ThumImg { get; set; }

        /// <summary>
        /// Desc:视频链接地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string VideoUrl { get; set; }

        /// <summary>
        /// Desc:是否置顶
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsTop { get; set; } = false;

        /// <summary>
        /// Desc:是否热点
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsHot { get; set; } = false;

        /// <summary>
        /// Desc:是否允许评论
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsComment { get; set; } = false;
        /// <summary>
        /// Desc:审核状态
        /// Default:b'1'
        /// Nullable:False
        /// </summary>
        public bool Audit { get; set; } = true;

        /// <summary>
        /// SEO关键字
        /// </summary>
        [Required(ErrorMessage = "文章SEO关键字不能为空")]
        public string KeyWord { get; set; }

        /// <summary>
        /// 文章摘要
        /// </summary>
        [Required(ErrorMessage = "文章摘要不能为空")]
        public string Summary { get; set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        [Required(ErrorMessage = "文章内容不能为空")]
        public string Content { get; set; }
    }
    public class ArticleModifyInput : ArticleInput, IEntity
    {
        [Required(ErrorMessage = "文章Id不能为空")]
        public int Id { get; set; }
    }
}
