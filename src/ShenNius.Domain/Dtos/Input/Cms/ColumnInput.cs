﻿using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Common;
using System.ComponentModel.DataAnnotations;

/*************************************
* 类 名： ColumnInput
* 作 者： realyrare
* 邮 箱： mahonggang8888@126.com
* 时 间： 2021/3/15 19:04:32
* .netV： 3.1
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Domain.Dtos.Input.Cms
{
    public class ColumnInput : BaseTenant
    {
        [Required(ErrorMessage = "标题必须填写")]
        public string Title { get; set; }
        public string EnTitle { get; set; }
        public string SubTitle { get; set; }
        public int ParentId { get; set; } = 0;
        public string ParentList { get; set; }
        public int Layer { get; set; }
        public string Attr { get; set; }
        public string ImgUrl { get; set; }
        [Required(ErrorMessage = "站点关键字必须填写")]
        public string Keyword { get; set; }
        [Required(ErrorMessage = "站点描述必须填写")]
        public string Summary { get; set; }
    }
    public class ColumnModifyInput : ColumnInput, IEntity
    {
        [Required(ErrorMessage = "栏目Id必须填写")]
        public int Id { get; set; }
    }
}