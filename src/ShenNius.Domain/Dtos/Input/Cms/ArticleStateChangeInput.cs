﻿using ShenNius.Domain.Dtos.Common;
using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Cms
{
    public class ArticleStateChangeInput : BaseTenant
    {
        [Required(ErrorMessage = "文章Id不能为空")]
        public int Id { get; set; }
        [Required(ErrorMessage = "要改变的状态类型不能为空")]
        public string Type { get; set; }
        public bool StateValue { get; set; }
    }
}
