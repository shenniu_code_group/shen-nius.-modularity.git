﻿using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Common;
using System.ComponentModel.DataAnnotations;

/*************************************
* 类名：KeywordInput
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/3/31 19:22:32
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Domain.Dtos.Input.Cms
{
    public class KeywordInput : BaseTenant
    {
        [Required(ErrorMessage = "关键词名称必须填写")]
        public string Title { get; set; }
        [Required(ErrorMessage = "关键词Url必须填写")]
        public string Url { get; set; }
    }
    public class KeywordModifyInput : KeywordInput, IEntity
    {
        [Required(ErrorMessage = "关键词Id必须填写")]
        public int Id { get; set; }
    }
}