﻿using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Common;
using System.ComponentModel.DataAnnotations;

/*************************************
* 类 名： AdvListInput
* 作 者： realyrare
* 邮 箱： mahonggang8888@126.com
* 时 间： 2021/3/16 17:31:39
* .netV： 3.1
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Domain.Dtos.Input.Cms
{
    public class AdvListInput : BaseTenant
    {
        /// <summary>
        /// Desc:广告位名称
        /// Default:-
        /// Nullable:False
        /// </summary>
        [Required(ErrorMessage = "广告位名称不能为空")]
        public string Title { get; set; }

        /// <summary>
        /// Desc:广告位类型
        /// Default:0
        /// Nullable:False
        /// </summary>
        [Required(ErrorMessage = "广告位类型不能为空")]
        public int Type { get; set; }

        /// <summary>
        /// Desc:图片地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        [Required(ErrorMessage = "图片地址不能为空")]
        public string ImgUrl { get; set; }

        /// <summary>
        /// Desc:链接地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string LinkUrl { get; set; }

        /// <summary>
        /// Desc:打开窗口类型
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Target { get; set; } = "_target";

        /// <summary>
        /// Desc:是否启用时间显示
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsTimeLimit { get; set; } = false;

        /// <summary>
        /// Desc:开始时间
        /// Default:-
        /// Nullable:True
        /// </summary>
        public DateTime? BeginTime { get; set; }

        /// <summary>
        /// Desc:结束时间
        /// Default:-
        /// Nullable:True
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// Desc:排序
        /// Default:0
        /// Nullable:False
        /// </summary>
        public int Sort { get; set; } = 0;
        [Required(ErrorMessage = "备注不能为空")]
        public string Summary { get; set; }

    }
    public class AdvListModifyInput : AdvListInput, IEntity
    {
        [Required(ErrorMessage = "Id不能为空")]
        public int Id { get; set; }
    }
}