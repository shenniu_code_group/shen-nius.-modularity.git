﻿using ShenNius.Domain.Dtos.Common;
using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Shop
{
    public class CategoryInput : BaseTenant
    {
        public int ParentId { get; set; }
        public string IconSrc { get; set; }
        /// </summary>
        [Required(ErrorMessage = "商品分类不能为空")]
        public string Name { get; set; }
    }
    public class CategoryModifyInput : CategoryInput
    {
        [Required(ErrorMessage = "商品分类id必须大于0")]
        public int Id { get; set; }
    }
}
