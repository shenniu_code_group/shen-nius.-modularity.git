﻿using Newtonsoft.Json;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Shop;
using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Shop
{
    public class GoodsInput : BaseTenant
    {
        [Required(ErrorMessage = "商品名称必须填写")]
        public string Name { get; set; }
        [Required(ErrorMessage = "商品分类必须选择")]
        public int CategoryId { get; set; }
        /// <summary>
        /// 商品规格
        /// </summary>
        [Required(ErrorMessage = "商品规格必须选择")]
        public int SpecType { get; set; }
        /// <summary>
        /// 库存计算方式 
        /// </summary>
        [Required(ErrorMessage = "库存计算方式必须选择")]
        public int DeductStockType { get; set; }
        [Required(ErrorMessage = "商品详情必须填写")]
        public string Content { get; set; }
        /// <summary>
        /// 初始销量
        /// </summary>
        public int SalesInitial { get; set; }
        /// <summary>
        /// 实际销量
        /// </summary>
        public int SalesActual { get; set; }
        /// <summary>
        /// 配送模板id
        /// </summary>

        public int DeliveryId { get; set; }

        public DateTime CreateTime { get; set; } = DateTime.Now;

        /// <summary>
        /// 商品状态 
        /// </summary>
        [Required(ErrorMessage = "商品状态必须选择")]
        public int GoodsStatus { get; set; }
        [Required(ErrorMessage = "商品图片至少需要上传一张")]
        public string ImgUrl { get; set; }
        /// <summary>
        /// 商品多规格
        /// </summary>
        public string SpecMany { get; set; }
        /// <summary>
        /// 单规格
        /// </summary>
        public string SpecSingle { get; set; }
        public GoodsSpecInput GoodsSpecInput { get; set; }

        /// <summary>
        /// 但规格构建实体数据
        /// </summary>
        /// <param name="goodsId"></param>
        /// <returns></returns>
        public GoodsSpec BuildGoodsSpec(int goodsId)
        {
            GoodsSpecInput.CreateTime = CreateTime;
            GoodsSpecInput.TenantId = TenantId;
            GoodsSpecInput.GoodsId = goodsId;
            GoodsSpecInput.SpecSkuId = null;
            return GoodsSpec.Create(GoodsSpecInput);
        }


        /// <summary>
        /// 多规格构建实体数据
        /// </summary>
        /// <param name="goodsId"></param>
        /// <returns></returns>
        public List<GoodsSpec> BuildGoodsSpecs(int goodsId)
        {
            var list = new List<GoodsSpec>();
            var specMany = JsonConvert.DeserializeObject<SpecManyInput>(SpecMany);
            foreach (var specList in specMany.SpecList)
            {
                specList.GoodsSpec.SpecSkuId = specList.SpecSkuId;
                specList.GoodsSpec.GoodsId = goodsId;
                specList.GoodsSpec.CreateTime = CreateTime;
                specList.GoodsSpec.TenantId = TenantId;
                var goodsSpec = GoodsSpec.Create(specList.GoodsSpec);
                list.Add(goodsSpec);
            }
            return list;
        }
        /// <summary>
        /// 构建商品多规格关系实体
        /// </summary>
        /// <param name="goodsId"></param>
        /// <returns></returns>
        public List<GoodsSpecRel> BuildGoodsSpecRels(int goodsId)
        {
            var list = new List<GoodsSpecRel>();
            var specMany = JsonConvert.DeserializeObject<SpecManyInput>(SpecMany);
            foreach (var specList in specMany.SpecList)
            {
                foreach (var goodsSpecRelDto in specList.GoodsSpecRels)
                {
                    goodsSpecRelDto.GoodsId = goodsId;
                    goodsSpecRelDto.CreateTime = CreateTime;
                    goodsSpecRelDto.TenantId = TenantId;
                    var model = GoodsSpecRel.Create(goodsSpecRelDto.GoodsId, goodsSpecRelDto.TenantId, goodsSpecRelDto.SpecId, goodsSpecRelDto.SpecValueId);
                    list.Add(model);
                }
            }
            return list;
        }
    }
}
