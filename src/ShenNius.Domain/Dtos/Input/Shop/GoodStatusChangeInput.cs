﻿using ShenNius.Domain.Dtos.Common;
using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Shop
{
    public class GoodStatusChangeInput : BaseTenant
    {
        [Required(ErrorMessage = "商品Id不能为空")]
        public int Id { get; set; }
        public int GoodsStatus { get; set; }
    }
}
