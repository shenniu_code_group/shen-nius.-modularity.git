﻿using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Input.Shop
{
    public class GoodsModifyInput : GoodsInput
    {
        [Required(ErrorMessage = "商品Id不能为空")]
        public int Id { get; set; }

        public DateTime ModifyTime { get; set; } = DateTime.Now;
    }
}
