﻿using ShenNius.Domain.Dtos.Common;
using System.ComponentModel.DataAnnotations;

/*************************************
* 类名：SpecInput
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/12 17:25:37
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Domain.Dtos.Input.Shop
{
    public class SpecInput : BaseTenant
    {
        [Required(ErrorMessage = "商品规格名称不能为空")]
        public string SpecName { get; set; }
        [Required(ErrorMessage = "商品规格值不能为空")]
        public string SpecValue { get; set; }
    }
    /// <summary>
    /// 根据规格组id添加规格值
    /// </summary>
    public class SpecValuesInput : BaseTenant
    {
        [Required(ErrorMessage = "商品规格Id不能为空")]
        public int SpecId { get; set; }
        [Required(ErrorMessage = "商品规格值不能为空")]
        public string SpecValue { get; set; }
    }

}