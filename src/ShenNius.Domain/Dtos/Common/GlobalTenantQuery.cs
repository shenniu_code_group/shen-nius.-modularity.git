﻿using ShenNius.Domain.Entity.Common;

namespace ShenNius.Domain.Dtos.Common
{
    public class GlobalTenantQuery : IGlobalTenant
    {
        public int TenantId { get; set; }
    }
}
