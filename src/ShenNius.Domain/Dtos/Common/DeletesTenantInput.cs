﻿using ShenNius.Domain.Entity.Common;
using System.ComponentModel.DataAnnotations;

namespace ShenNius.Domain.Dtos.Common
{
    public class DeletesInput
    {
        [Required(ErrorMessage = "删除Id不能为空")]
        public List<int> Ids { get; set; } = new List<int>();
    }
    public class DeletesTenantInput : DeletesInput, IGlobalTenant
    {

        public int TenantId { get; set; }
    }
}
