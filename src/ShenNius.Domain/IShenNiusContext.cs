﻿using ShenNius.Domain.Dtos.Output.Sys;
using SqlSugar;
using System.Security.Claims;

namespace ShenNius.Domain
{
    /// <summary>
    /// 用户上下文
    /// </summary>
    public interface IShenNiusContext
    {
        ISqlSugarClient Db { get; }
        /// <summary>
        /// 获取当前用户信息
        /// </summary>
        /// <returns></returns>
        UserOutput GetCurrentUserInfo();
        /// <summary>
        /// 当前用户租户Id
        /// </summary>
        int TenantId { get; }
        string TenantName { get; }
        /// <summary>
        /// 当前用户
        /// </summary>
        string Name { get; }

        /// <summary>
        ///当前用户id
        /// </summary>
        int Id { get; }

        /// <summary>
        /// 是否登录
        /// </summary>
        /// <returns></returns>
        bool IsAuthenticated();
        IEnumerable<Claim> GetClaimsIdentity();
        List<string> GetClaimValueByType(string claimType);

        string GetToken();
        List<string> GetUserInfoFromToken(string claimType);
        /// <summary>
        /// 是否是超级管理员，超级管理员拥有所有租户数据权限
        /// </summary>
        /// <returns></returns>
        bool IsAdmin();
        /// <summary>
        /// 当前系统位置ip
        /// </summary>
        string Ip { get; }
        /// <summary>
        /// 当前系统位置
        /// </summary>
        string Address { get; }
        /// <summary>
        /// 持久化操作日志
        /// </summary>
        /// <param name="msg">操作内容</param>
        /// <param name="userId">当前登录用户Id，只有用户在登录时需要传递userId,登陆后不需要传值，系统自动赋值</param>
        /// <param name="application"></param>
        void WriteLog(string msg, int userId = 0, string application = null, int tenantId = 0);
        // string GetTenanNameById(int tenantId);
        /// <summary>
        /// 获取所有的租户
        /// </summary>
        /// <returns></returns>
        List<TenantOutput> GetAllTenans();
    }
}
