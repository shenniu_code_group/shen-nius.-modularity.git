﻿namespace ShenNius.Domain.Entity.Common
{
    public interface IHasModifyTime
    {
        DateTime? ModifyTime { get; }
    }
}
