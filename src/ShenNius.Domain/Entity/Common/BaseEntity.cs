﻿using SqlSugar;

/*************************************
* 类名：Common
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/3/30 17:24:54
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Domain.Entity.Common
{

    /// <summary>
    /// 非多租户领域基类
    /// </summary>
    public class BaseEntity : IEntity, IHasModifyTime, IHasCreateTime, IHasDeleteTime, ISoftDelete
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; protected set; }
        public DateTime? ModifyTime { get; protected set; }
        /*private 是完全私有的,只有当前类中的成员能访问到.
        protected 是受保护的,只有当前类的成员与继承该类的类才能访问 故这里申明为protected，不然外界无法赋值*/
        public DateTime CreateTime { get; protected set; } = DateTime.Now;
        public bool IsDeleted { get; set; }
        public DateTime? DeleteTime { get; set; }
        /*sql sugar更新时直接使用对象调用删除的方法，所有的实体都会有值或空值且不支持，故对删除相关两个属性取消private设置*/
        public void SoftDelete()
        {
            /*该方式更新使用需要先查询再赋值*/
            this.IsDeleted = true;
            this.DeleteTime = DateTime.Now;
        }
        public void NotifyModified()
        {
            this.ModifyTime = DateTime.Now;
        }
    }

    /// <summary>
    ///多租户领域基类
    /// </summary>
    public class BaseTenantEntity : BaseEntity, IGlobalTenant
    {
        public int TenantId { get; set; }
        [SugarColumn(IsIgnore = true)]
        public string TenantName { get; set; }

    }
    public class BaseTenantTreeEntity : BaseTenantEntity
    {
        public string ParentList { get; protected set; }
        /// Desc:栏位等级     
        public int Layer { get; protected set; }
        public int ParentId { get; protected set; }
        public void ChangeParentList(int layer, string parentList)
        {
            Layer = layer;
            ParentList = parentList;
            NotifyModified();
        }
    }





}