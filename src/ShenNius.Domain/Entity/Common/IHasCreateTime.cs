﻿namespace ShenNius.Domain.Entity.Common
{
    public interface IHasCreateTime
    {
        DateTime CreateTime { get; }
    }
}
