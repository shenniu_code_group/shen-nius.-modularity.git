﻿namespace ShenNius.Domain.Entity.Common
{
    public interface IEntity
    {
        int Id { get; }
    }
}
