﻿namespace ShenNius.Domain.Entity.Common
{
    public interface IHasDeleteTime
    {
        DateTime? DeleteTime { get; }
    }
}
