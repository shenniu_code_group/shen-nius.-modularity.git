﻿using ShenNius.Common.Hepler;
using ShenNius.Domain.Dtos.Input.Sys;
using ShenNius.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Domain.Entity.Sys
{
    [SugarTable("Sys_User")]
    public class User : BaseTenantEntity, IAggregateRoot
    {
        /// <summary>
        /// 登录账号
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// 登录密码
        /// </summary>
        public string Password { get; private set; }

        /// <summary>
        /// 真是姓名
        /// </summary>
        public string TrueName { get; private set; }
        /// <summary>
        /// 头像
        /// </summary>
        public string HeadImg { get; private set; }
        /// <summary>
        /// 性别
        /// </summary>
        public string Sex { get; private set; } = "男";
        /// <summary>
        /// 手机号码
        /// </summary>
        public string Mobile { get; private set; }
        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; private set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; private set; }
        public string Ip { get; private set; }
        public string Address { get; private set; }
        /// <summary>
        /// 当前登录时间
        /// </summary>
        public DateTime? LastLoginTime { get; set; }
        public bool IsLogin { get; set; }
        public void Create()
        {
            EncryptPassword();
            ModifyIpAddress();
        }
        public void EncryptPassword()
        {
            if (string.IsNullOrEmpty(Password))
            {
                throw new ArgumentException(nameof(Password));
            }
            Password = Md5Crypt.Encrypt(Password);
            if (Id > 0)
            {
                NotifyModified();
            }
        }
        public void IsEquaPassword(string oldPassword, string confirmPassword)
        {
            if (string.IsNullOrEmpty(oldPassword))
            {
                throw new ArgumentException(nameof(oldPassword));
            }

            if (string.IsNullOrEmpty(confirmPassword))
            {
                throw new ArgumentException(nameof(confirmPassword));
            }

            oldPassword = Md5Crypt.Encrypt(oldPassword);
            if (Password != oldPassword)
            {
                throw new ArgumentException("旧密码错误!");
            }
            Password = Md5Crypt.Encrypt(confirmPassword);
            if (Password == oldPassword)
            {
                throw new ArgumentException("新密码与旧密码相同，请设置不一样的密码!");
            }
        }
        public void ModifyLoginInfo()
        {
            ModifyIpAddress();
            ModifyIsLogin();
        }
        public void ModifyIpAddress()
        {
            var ip = MyHttpContext.Current.Request.Headers["X-Forwarded-For"].FirstOrDefault() ?? MyHttpContext.Current.Connection.RemoteIpAddress.ToString();
            Ip = ip;
            Address = IpParseHelper.GetAddressByIP(Ip);
        }

        public void ModifyIsLogin()
        {
            IsLogin = true;
            LastLoginTime = DateTime.Now;
        }
        public void Modify(UserModifyInput input)
        {
            if (!string.IsNullOrEmpty(input.Name) && Name != input.Name)
            {
                Name = input.Name;
            }
            if (!string.IsNullOrEmpty(input.Email) && Email != input.Email)
            {
                Email = input.Email;
            }
            if (!string.IsNullOrEmpty(input.TrueName) && TrueName != input.TrueName)
            {
                TrueName = input.TrueName;
            }
            if (!string.IsNullOrEmpty(input.Remark))
            {
                Remark = input.Remark;
            }
            if (!string.IsNullOrEmpty(input.Sex) && Sex != input.Sex)
            {
                Sex = input.Sex;
            }
            NotifyModified();
        }
    }
}
