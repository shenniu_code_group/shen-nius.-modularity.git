﻿using ShenNius.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Domain.Entity.Sys
{
    ///<summary>
    /// 权限角色表
    ///</summary>
    [SugarTable("Sys_Role")]
    public partial class Role : BaseTenantEntity, IAggregateRoot
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public void Modify(int id, string name, string description)
        {
            if (!string.IsNullOrEmpty(name) && Name != name)
            {
                Name = name;
            }

            if (!string.IsNullOrEmpty(description))
            {
                Description = description;
            }
            Id = id;
            NotifyModified();
        }
    }
}
