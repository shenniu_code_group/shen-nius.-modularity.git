﻿using ShenNius.Domain.Dtos.Input.Sys;
using ShenNius.Domain.Entity.Common;
using SqlSugar;

/*************************************
* 类名：Tenant
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/3/11 17:10:38
*┌───────────────────────────────────┐　    
*│　      版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Domain.Entity.Sys
{
    [SugarTable("Sys_Tenant")]
    public class Tenant : BaseEntity, IAggregateRoot
    {

        /// <summary>
        /// Desc:网站名称
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Desc:网站域名
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Url { get; private set; }

        /// <summary>
        /// Desc:网站描述
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Summary { get; private set; }

        /// <summary>
        /// Desc:公司电话
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Tel { get; private set; }


        /// <summary>
        /// Desc:公司人事邮箱
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// Desc:公司客服QQ
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string QQ { get; private set; }

        /// <summary>
        /// Desc:微信公众号图片
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string WeiXin { get; private set; }

        /// <summary>
        /// Desc:微博链接地址或者二维码
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string WeiBo { get; private set; }

        /// <summary>
        /// Desc:公司地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Address { get; private set; }

        /// <summary>
        /// Desc:网站备案号其它等信息
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Code { get; private set; }

        /// <summary>
        /// Desc:网站SEO标题
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Desc:网站SEO关键字
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Keyword { get; private set; }

        /// <summary>
        /// Desc:网站SEO描述
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Desc:网站版权等信息
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Copyright { get; private set; }

        public bool IsCurrent { get; set; }

        public void Modify(TenantModifyInput input)
        {
            if (!string.IsNullOrEmpty(input.Name) && Name != input.Name)
            {
                Name = input.Name;
            }
            if (!string.IsNullOrEmpty(input.Url))
            {
                Url = input.Url;
            }
            if (!string.IsNullOrEmpty(input.Summary))
            {
                Summary = input.Summary;
            }
            if (!string.IsNullOrEmpty(input.Keyword))
            {
                Keyword = input.Keyword;
            }
            if (!string.IsNullOrEmpty(input.Title))
            {
                Title = input.Title;
            }
            if (!string.IsNullOrEmpty(input.Description))
            {
                Description = input.Description;
            }
            if (!string.IsNullOrEmpty(input.WeiBo))
            {
                WeiBo = input.WeiBo;
            }
            if (!string.IsNullOrEmpty(input.WeiXin))
            {
                WeiXin = input.WeiXin;
            }
            if (!string.IsNullOrEmpty(input.QQ))
            {
                QQ = input.QQ;
            }
            if (!string.IsNullOrEmpty(input.Address))
            {
                Address = input.Address;
            }
            if (!string.IsNullOrEmpty(input.Tel))
            {
                Tel = input.Tel;
            }
            if (!string.IsNullOrEmpty(input.Copyright))
            {
                Copyright = input.Copyright;
            }
            if (!string.IsNullOrEmpty(input.Code))
            {
                Code = input.Code;
            }
            if (!string.IsNullOrEmpty(input.Email))
            {
                Email = input.Email;
            }
            //if (IsCurrent!=input.IsCurrent)
            //{
            //    IsCurrent = input.IsCurrent;
            //}
            //if (UserId != input.UserId)
            //{
            //    UserId = input.UserId;
            //}
            NotifyModified();
        }
    }
}