﻿using ShenNius.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Domain.Entity.Sys
{
    ///<summary>
    ///
    ///</summary>
    [SugarTable("Sys_R_User_Role")]
    public partial class R_User_Role : BaseTenantEntity
    {
        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int UserId { get; private set; }

        /// <summary>
        /// Desc:
        /// Default:
        /// Nullable:False
        /// </summary>           
        public int RoleId { get; private set; }


        public static R_User_Role Create(int userId, int roleId)
        {
            R_User_Role addModel = new R_User_Role() { UserId = userId, RoleId = roleId };
            return addModel;
        }
    }
}
