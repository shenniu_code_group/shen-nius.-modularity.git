﻿using Microsoft.Extensions.DependencyInjection;
using ShenNius.Common.Hepler;
using ShenNius.Domain.Entity.Common;
using SqlSugar;

namespace ShenNius.Domain.Entity.Sys
{
    ///<summary>
    /// 系统操作表
    ///</summary>
    [SugarTable("Sys_Log")]
    public partial class Log : IAggregateRoot, IGlobalTenant, IHasCreateTime
    {
        /// <summary>
        /// Desc:唯一标号Guid
        /// </summary>           
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; private set; }
        /// <summary>
        /// 应用程序
        /// </summary>
        public string Application { get; set; } = "api";
        /// <summary>
        /// 时间
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.Now;
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 请求Url
        /// </summary>
        public string Callsite { get; set; }
        /// <summary>
        /// IP地址
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// 认证用户名
        /// </summary>
        [SugarColumn(IsIgnore = true)]
        public string UserName { get; set; }
        /// <summary>
        /// 浏览器信息
        /// </summary>
        public string Browser { get; set; }
        public int TenantId { get; set; }
        public int UserId { get; set; }
        public string Address { get; set; }

        public static Log Create(string msg, int userId = 0, string application = null, int tenantId = 0)
        {
            if (string.IsNullOrEmpty(msg))
            {
                throw new ArgumentException($"“{nameof(msg)}”不能为 null 或空。", nameof(msg));
            }
            var sc = MyHttpContext.Current.RequestServices.GetRequiredService<IShenNiusContext>();
            Log log = new Log()
            {
                Callsite = MyHttpContext.Current.Request.Path + MyHttpContext.Current.Request.QueryString,
                IP = sc.Ip,
                Address = sc.Address,
                Browser = MyHttpContext.Current.Request.Headers["User-Agent"].FirstOrDefault(),
            };
            if (tenantId > 0)
            {
                log.TenantId = tenantId;
                log.Message = msg;
            }
            else
            {
                if (tenantId != 0)
                {
                    log.TenantId = sc.TenantId;
                    log.Message = $"当前租户:{sc.TenantName},{msg};";
                }
                else
                {
                    log.Message = $"{msg};";
                }
            }
            if (string.IsNullOrEmpty(log.Application))
            {
                log.Application = application;
            }
            if (userId == 0)
            {
                log.UserId = sc.Id;
            }
            else
            {
                log.UserId = userId;
            }
            return log;
        }
    }
}
