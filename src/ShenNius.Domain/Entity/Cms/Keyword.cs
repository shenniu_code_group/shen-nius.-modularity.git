﻿using ShenNius.Domain.Entity.Common;
using SqlSugar;

/*************************************
* 类名：Keyword
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/3/31 19:03:29
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Domain.Entity.Cms
{
    /// <summary>
    /// 关键词
    /// </summary>
    [SugarTable("Cms_Keyword")]
    public class Keyword : BaseTenantEntity, IAggregateRoot
    {
        public string Title { get; private set; }
        public string Url { get; private set; }
        public static Keyword Create(string title, string url, int tenantId)
        {
            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentException($"“{nameof(title)}”不能为 null 或空。", nameof(title));
            }

            if (string.IsNullOrEmpty(url))
            {
                throw new ArgumentException($"“{nameof(url)}”不能为 null 或空。", nameof(url));
            }

            var model = new Keyword()
            {
                Title = title,
                Url = url,
                TenantId = tenantId
            };
            return model;
        }
    }
}