﻿using ShenNius.Common;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Dtos.Input.Shop;
using ShenNius.Domain.Entity.Shop;
using ShenNius.Repository;
using SqlSugar;
using System.Linq.Expressions;

namespace ShenNius.Domain.Repository.Shop
{
    public interface IGoodsRepository : IBaseRepository<Goods>
    {
        Task<ApiResult> AddAsync(GoodsInput input);
        Task<ApiResult> ModifyAsync(GoodsModifyInput input);
        Task<ApiResult<GoodsModifyInput>> DetailAsync(DetailTenantQuery query);
        /// <summary>
        /// 添加规格组名称和值
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ApiResult> AddSpecAsync(SpecInput input);
        /// <summary>
        /// 添加规格组值
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ApiResult> AddSpecAsync(SpecValuesInput input);

        Task<ApiResult> GetListPageAsync(KeyListTenantQuery query);
        /// <summary>
        /// 小程序首页
        /// </summary>
        /// <returns></returns>
        Task<ApiResult> GetByWherePageAsync(ListTenantQuery query, Expression<Func<Goods, Category, GoodsSpec, object>> orderBywhere, OrderByType sort, Expression<Func<Goods, Category, GoodsSpec, bool>> where = null);
        /// <summary>
        /// 立即购买
        /// </summary>
        /// <returns></returns>
        Task<ApiResult> GetBuyNowAsync(int goodsId, int goodsNum, string goodsNo, int tenantId);
        /// <summary>
        /// 商品信息判断（添加购物车，下单共用）
        /// </summary>
        /// <param name="goodsId"></param>
        /// <param name="goodsNum"></param>
        /// <param name="specSkuId"></param>
        /// <param name="appUserId"></param>
        /// <returns></returns>
        Task<Tuple<Goods, GoodsSpec>> GoodInfoIsExist(int goodsId, int goodsNum, string specSkuId, int appUserId);
    }
}
