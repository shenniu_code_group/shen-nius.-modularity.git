﻿using ShenNius.Common;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Dtos.Output.Shop;
using ShenNius.Domain.Dtos.Query.Shop;
using ShenNius.Domain.Entity.Shop;
using ShenNius.Repository;

namespace ShenNius.Domain.Repository.Shop
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        Task<ApiResult> GetListPageAsync(OrderKeyListTenantQuery query);
        Task<ApiResult<OrderDetailOutput>> GetOrderDetailAsync(DetailTenantQuery query);
    }
}
