﻿using ShenNius.Common;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Common;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Repository;

namespace ShenNius.Domain.Repository.Sys
{
    public interface IRecycleRepository : IBaseRepository<Recycle>
    {
        Task<ApiResult> RestoreAsync(DeletesInput deletesInput);
        Task<ApiResult> RealyDeleteAsync(DeletesInput deletesInput);
        Task<ApiResult> SoftDeleteAsync<TEntity>(DeletesTenantInput input, IBaseRepository<TEntity> service) where TEntity : BaseTenantEntity, new();
        Task<ApiResult> SoftDeleteAsync<TEntity>(DeletesInput input, IBaseRepository<TEntity> service) where TEntity : BaseEntity, new();
        Task<ApiResult> GetPagesAsync(KeyListTenantQuery query);
    }
}
