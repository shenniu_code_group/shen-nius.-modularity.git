﻿namespace ShenNius.Domain.ValueObjects
{
    public class ConfigType
    {
        /// <summary>
        /// 系统配置
        /// </summary>
        public string Setting;
        /// <summary>
        /// 包邮
        /// </summary>
        public string Freight;
    }
}
