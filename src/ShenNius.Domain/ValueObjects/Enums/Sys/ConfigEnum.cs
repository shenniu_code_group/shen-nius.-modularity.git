﻿using System.ComponentModel;

namespace ShenNius.Domain.ValueObjects.Enum
{
    /// <summary>
    /// 字典表配置类型
    /// </summary>
    public enum ConfigEnum
    {
        /// <summary>
        /// 菜单按钮
        /// </summary>
        [Description("按钮")]
        Button,
        /// <summary>
        /// 包邮
        /// </summary>
        [Description("包邮")]
        Freight,
        /// <summary>
        /// 系统配置
        /// </summary>
        [Description("系统配置")]
        Setting,
    }
}
