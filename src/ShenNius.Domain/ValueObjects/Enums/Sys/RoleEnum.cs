﻿using System.ComponentModel;

namespace ShenNius.Domain.ValueObjects.Enums.Sys
{
    /// <summary>
    /// 系统户级别
    /// </summary>
    public enum RoleEnum
    {
        /// <summary>
        /// 超级管理员
        /// </summary>
        [Description("超级管理员")]
        SuperAdmin = 1,
        /// <summary>
        /// 管理员
        /// </summary>
        [Description("管理员")]
        Admin = 2,
        /// <summary>
        /// 普通用户
        /// </summary>
        [Description("普通用户")]
        Normal = 3,
    }
}
