using Microsoft.AspNetCore.Builder;
using ShenNius.Admin.API;
using ShenNius.ModuleCore.Extensions;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddModule<ShenniusAdminApiModule>(builder.Configuration);
var app = builder.Build();
app.UseModule();
app.Run();
