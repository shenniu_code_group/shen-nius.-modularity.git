﻿using AutoMapper;
using ShenNius.Domain.Dtos.Input.Cms;
using ShenNius.Domain.Dtos.Input.Shop;
using ShenNius.Domain.Dtos.Input.Sys;
using ShenNius.Domain.Dtos.Output.Cms;
using ShenNius.Domain.Dtos.Output.Sys;
using ShenNius.Domain.Entity.Cms;
using ShenNius.Domain.Entity.Shop;
using ShenNius.Domain.Entity.Sys;

namespace ShenNius.Admin.API
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {

            //sys
            CreateMap<User, LoginOutput>().ForMember(d => d.LoginName, s => s.MapFrom(i => i.Name));
            CreateMap<User, UserOutput>();
            CreateMap<UserRegisterInput, User>();

            CreateMap<RoleInput, Role>();

            CreateMap<RoleModifyInput, Role>();

            CreateMap<MenuModifyInput, Menu>();
            CreateMap<MenuInput, Menu>();
            //ParentMenuOutput
            CreateMap<Menu, ParentMenuOutput>();
            CreateMap<Menu, MenuAuthOutput>();

            CreateMap<ConfigInput, Config>().ForMember(d => d.Value, s => s.MapFrom(i => i.Name));
            CreateMap<TenantInput, Tenant>();
            CreateMap<TenantModifyInput, Tenant>();


            //cms
            CreateMap<ColumnInput, Column>();
            CreateMap<ColumnModifyInput, Column>();

            CreateMap<ArticleInput, Article>();
            CreateMap<ArticleModifyInput, Article>();

            CreateMap<AdvListInput, AdvList>();
            CreateMap<AdvListModifyInput, AdvList>();

            CreateMap<KeywordInput, Keyword>();
            CreateMap<KeywordModifyInput, Keyword>();

            CreateMap<Keyword, KeywordOutput>();
            //shop
            CreateMap<GoodsInput, Goods>();
            CreateMap<GoodsModifyInput, Goods>();
            CreateMap<Goods, GoodsModifyInput>();

            CreateMap<GoodsSpec, GoodsSpecInput>();


            CreateMap<CategoryInput, Category>();
            CreateMap<CategoryModifyInput, Category>();
        }
    }
}
