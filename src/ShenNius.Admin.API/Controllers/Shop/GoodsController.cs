﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ShenNius.Common;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Dtos.Input.Shop;
using ShenNius.Domain.Repository.Shop;
using ShenNius.Domain.Repository.Sys;
using ShenNius.Domain.ValueObjects.Enum;
using ShenNius.FileManagement;
using ShenNius.Infrastructure.Attributes;
using System.Threading.Tasks;

namespace ShenNius.Shop.API.Controllers
{
    /// <summary>
    /// 商品分类控制器
    /// </summary>
    [Route("api/shop/[controller]/[action]")]
    [MultiTenant]
    [ApiController]
    public class GoodsController : ControllerBase
    {
        private readonly IGoodsRepository _goodsRepository;
        private readonly IUploadFile _uploadHelper;
        private readonly IRecycleRepository _recycleRepository;

        public GoodsController(IGoodsRepository goodsRepository, IUploadFile uploadHelper, IRecycleRepository recycleRepository)
        {
            _goodsRepository = goodsRepository;
            _uploadHelper = uploadHelper;
            _recycleRepository = recycleRepository;
        }

        [HttpGet, Authority]
        public Task<ApiResult> GetListPages([FromQuery] KeyListTenantQuery query)
        {
            return _goodsRepository.GetListPageAsync(query);
        }
        [HttpPost, Authority]
        public async Task<ApiResult> Add([FromBody] GoodsInput input)
        {
            return await _goodsRepository.AddAsync(input);
        }
        [HttpPut, Authority]
        public async Task<ApiResult> Modify([FromBody] GoodsModifyInput input)
        {
            return await _goodsRepository.ModifyAsync(input);
        }
        [HttpPost]
        public Task<ApiResult> AddSpec([FromForm] SpecInput input)
        {
            return _goodsRepository.AddSpecAsync(input);
        }
        [HttpPost]
        public Task<ApiResult> AddSpecValue([FromForm] SpecValuesInput input)
        {
            return _goodsRepository.AddSpecAsync(input);
        }
        [HttpPost, AllowAnonymous]
        public IActionResult UploadImg()
        {
            var files = Request.Form.Files[0];
            var result = _uploadHelper.Upload(files, "goods/");
            //TinyMCE 指定的返回格式
            return Ok(new { location = result });
        }
        [HttpPost]
        public ApiResult MultipleUploadImg([FromForm] IFormCollection formData)
        {
            var data = _uploadHelper.Upload(formData.Files, "goods/");
            //TinyMCE 指定的返回格式
            return new ApiResult(data);
        }

        /// <summary>
        /// 单个彻底删除
        /// </summary>
        /// <param name="deleteInput"></param>
        /// <returns></returns>
        [HttpDelete, Authority]
        public virtual async Task<ApiResult> Delete([FromBody] DeletesTenantInput deleteInput)
        {
            var res = await _goodsRepository.DeleteAsync(deleteInput.Ids);
            return res <= 0 ? new ApiResult("删除失败了！") : new ApiResult();
        }
        /// <summary>
        /// 软删除并将内容放回到回收站
        /// </summary>
        /// <param name="deleteInput"></param>
        /// <returns></returns>
        [HttpDelete, Authority(Action = nameof(BtnEnum.Recycle))]
        public virtual Task<ApiResult> SoftDelete([FromBody] DeletesTenantInput deleteInput)
        {
            return _recycleRepository.SoftDeleteAsync(deleteInput, _goodsRepository);
        }

        [HttpPost]
        public async Task<ApiResult> ChangeState([FromBody] GoodStatusChangeInput input)
        {
            var model = await _goodsRepository.GetModelAsync(d => !d.IsDeleted && d.Id == input.Id && d.TenantId == input.TenantId);
            if (model == null || model.Id <= 0)
            {
                return new ApiResult("该条数据已不存在！");
            }
            model.ChangeGoodStatus(input.GoodsStatus);
            var res = await _goodsRepository.UpdateAsync(model, d => new { d.CreateTime });
            return res > 0 ? ApiResult.Successed("状态更改成功") : new ApiResult("状态更改失败");
        }
    }
}
