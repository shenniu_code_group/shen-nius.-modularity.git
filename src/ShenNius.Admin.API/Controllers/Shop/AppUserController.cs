﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Admin.API.Controllers;
using ShenNius.Common;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Shop;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Infrastructure.Attributes;
using ShenNius.Repository.Extensions;
using SqlSugar;
using System.Threading.Tasks;

/*************************************
* 类名：AppUserController
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/23 11:01:15
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Shop.API.Controllers
{
    [Route("api/shop/[controller]/[action]")]
    [MultiTenant]
    public class AppUserController : ApiControllerBase
    {
        private readonly ISqlSugarClient _db;
        public AppUserController(ISqlSugarClient db)
        {
            _db = db;
        }

        [HttpGet, Authority]
        public async Task<ApiResult> GetListPages([FromQuery] KeyListTenantQuery query)
        {
            var res = await _db.Queryable<AppUser>().Where(d => d.TenantId == query.TenantId).WhereIF(!string.IsNullOrEmpty(query.Key), d => d.NickName.Equals(query.Key))
               .OrderBy(d => d.Id, OrderByType.Desc)
               .Select(d => new
               {
                   AvatarUrl = d.AvatarUrl,
                   OpenId = d.OpenId,
                   NickName = d.NickName,
                   Province = d.Province,
                   City = d.City,
                   TenantName = SqlFunc.Subqueryable<Tenant>().Where(s => s.Id == d.TenantId).Select(s => s.Name),
               }).ToPageAsync(query.Page, query.Limit);
            return new ApiResult(data: new { count = res.TotalItems, items = res.Items });
        }
    }
}