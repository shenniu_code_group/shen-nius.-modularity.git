﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Common;
using ShenNius.Domain.Dtos.Query.Shop;
using ShenNius.Domain.Repository.Shop;
using ShenNius.Infrastructure.Attributes;
using System.Threading.Tasks;

namespace ShenNius.Shop.API.Controllers
{
    /// <summary>
    /// 商品订单控制器
    /// </summary>
    [Route("api/shop/[controller]/[action]")]
    [MultiTenant]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository _orderRepository;

        public OrderController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        [HttpGet, Authority]
        public Task<ApiResult> GetListPages([FromQuery] OrderKeyListTenantQuery query)
        {
            return _orderRepository.GetListPageAsync(query);
        }

    }
}
