﻿/*************************************
* 类名：TenantController
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/3/11 17:22:57
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using ShenNius.Caches;
using ShenNius.Common;
using ShenNius.Common.Hepler;
using ShenNius.Domain;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Dtos.Input.Sys;
using ShenNius.Domain.Dtos.Output.Sys;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Domain.Repository.Sys;
using ShenNius.Domain.ValueObjects.Enum;
using ShenNius.Infrastructure.Attributes;
using ShenNius.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ShenNius.Admin.API.Controllers
{
    [Route("api/sys/[controller]/[action]")]
    public class TenantController : ApiBaseController<Tenant, DetailQuery, DeletesInput, KeyListQuery, TenantInput, TenantModifyInput>
    {
        private readonly IBaseRepository<Tenant> _repository;
        private readonly IDistributedCache _cache;
        private readonly IShenNiusContext _sc;
        private readonly IRecycleRepository _recycleRepository;
        private readonly SemaphoreSlim _slim;
        public TenantController(IBaseRepository<Tenant> repository, IMapper mapper, IDistributedCache cache, IShenNiusContext sc, IRecycleRepository recycleRepository) : base(repository, mapper)
        {
            _repository = repository;
            _cache = cache;
            _sc = sc;
            _recycleRepository = recycleRepository;
            _slim = new SemaphoreSlim(1);
        }
        /// <summary>
        /// 设置当前站点
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ApiResult> SetCurrent([FromBody] TenantCurrentInput input)
        {
            //把之前缓存存储的站点拿出来设置为不是当前的。
            await _slim.WaitAsync();
            try
            {
                var list = _sc.GetAllTenans();
                foreach (var item in list)
                {
                    if (item.Id == input.Id && !item.IsCurrent)
                    {
                        item.IsCurrent = true;
                    }
                    if (item.Id != input.Id && item.IsCurrent)
                    {
                        item.IsCurrent = false;
                    }
                }
                //目前只有超级管理员可以设置当前租户，设置后可以持久化到数据库，如果不持久化，默认就是超级管理员关联的租户
                _cache.Set($"{SysCacheKey.AllTenant}:{_sc.Id}", list);


                // var model = await _repository.GetModelAsync(d => d.Id == input.Id && d.IsCurrent == false && d.IsDeleted == false);
                //if (model == null)
                //{
                //    throw new ArgumentNullException("当前站点实体信息为空!");
                //}
                //var currentTenantId = _sc.TenantId;
                //if (currentTenantId > 0)
                //{
                //    await _repository.UpdateAsync(d => new Tenant() { IsCurrent = false }, d => d.Id == currentTenantId);
                //}

                //model.IsCurrent = true;
                //await _repository.UpdateAsync(model);
                //这里最好更新下缓存
                // _cache.Remove($"{SysCacheKey.AllTenant}:{_sc.Id}");
            }
            finally
            {
                _slim.Release();
            }
            return new ApiResult();
        }
        [HttpGet]
        public ApiResult GetList()
        {
            //首页加载该列表时赋值于缓存
            List<TenantOutput> list = new List<TenantOutput>();
            list = _sc.GetAllTenans();
            if (list.Count <= 0)
            {
                throw new ArgumentException("获取不到所有租户的信息");
            }
            if (!_sc.IsAdmin())
            {
                int currentTenantId = _sc.TenantId;
                var model = list.FirstOrDefault(d => d.IsCurrent == true && d.Id == currentTenantId);
                if (model == null)
                {
                    throw new ArgumentException("获取不到当前租户的信息");
                }
                list.Clear();
                list.Add(model);
            }
            return new ApiResult(data: list);
        }
        [HttpGet, Authority]
        public override async Task<ApiResult> GetListPages([FromQuery] KeyListQuery keyListQuery)
        {
            Expression<Func<Tenant, bool>> whereExpression = null;
            if (_sc.IsAdmin())
            {
                whereExpression = d => !d.IsDeleted;
                if (!string.IsNullOrEmpty(keyListQuery.Key))
                {
                    whereExpression = d => d.Title.Contains(keyListQuery.Key) && !d.IsDeleted;
                }
            }
            else
            {
                whereExpression = d => !d.IsDeleted && d.Id == _sc.TenantId;
                if (!string.IsNullOrEmpty(keyListQuery.Key))
                {
                    whereExpression = d => d.Title.Contains(keyListQuery.Key) && !d.IsDeleted && d.Id == _sc.TenantId;
                }
            }
            var res = await _repository.GetPagesAsync(keyListQuery.Page, keyListQuery.Limit, whereExpression, d => d.Id, false);
            return new ApiResult(data: new { count = res.TotalItems, items = res.Items });
        }
        [HttpDelete, Authority(Action = nameof(BtnEnum.Recycle))]
        public virtual Task<ApiResult> SoftDelete([FromBody] DeletesInput deleteInput)
        {
            _cache.Remove($"{SysCacheKey.AllTenant}:{_sc.Id}");
            return _recycleRepository.SoftDeleteAsync(deleteInput, _repository);
        }
        public override async Task<ApiResult> Modify([FromBody] TenantModifyInput input)
        {
            var model = await _repository.GetModelAsync(d => d.IsDeleted == false && d.Id == input.Id);
            if (model == null || (model?.Id) <= 0)
            {
                return new ApiResult($"该{input.Id}为查询到对应的数据！");
            }
            model.Modify(input);
            var res = await _repository.UpdateAsync(model, d => new { d.CreateTime });
            _cache.Remove($"{SysCacheKey.AllTenant}:{_sc.Id}");
            return res <= 0 ? new ApiResult("修改失败了！") : new ApiResult();
        }
        public override Task<ApiResult> Add([FromBody] TenantInput input)
        {
            _cache.Remove($"{SysCacheKey.AllTenant}:{_sc.Id}");
            return base.Add(input);
        }
    }
}