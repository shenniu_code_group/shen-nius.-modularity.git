﻿using Microsoft.AspNetCore.Mvc;

namespace ShenNius.Admin.API.Controllers
{
    [Route("api/sys/[controller]/[action]")]
    [ApiController]
    public abstract class ApiControllerBase : ControllerBase
    {

    }
}
