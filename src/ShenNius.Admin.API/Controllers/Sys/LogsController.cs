﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Common;
using ShenNius.Domain;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Infrastructure.Attributes;
using ShenNius.Repository;
using ShenNius.Repository.Extensions;
using SqlSugar;
using System.Threading.Tasks;

namespace ShenNius.Admin.API.Controllers
{
    public class LogsController : ApiControllerBase
    {
        private readonly IBaseRepository<Log> _logRepository;
        private readonly IShenNiusContext _sc;
        public LogsController(IBaseRepository<Log> logRepository, IShenNiusContext sc)
        {
            _logRepository = logRepository;
            _sc = sc;
        }

        [HttpDelete, Authority]
        public async Task<ApiResult> Deletes([FromBody] DeletesInput input)
        {
            return new ApiResult(await _logRepository.DeleteAsync(input.Ids));
        }

        [HttpGet, Authority]
        public async Task<ApiResult> GetListPages([FromQuery] KeyListQuery query)
        {
            var res = await _sc.Db.Queryable<Log>().Where(d => d.TenantId == _sc.TenantId)
               .WhereIF(!string.IsNullOrEmpty(query.Key), d => d.Message.Contains(query.Key))
                  .OrderBy(c => c.Id, OrderByType.Desc)
               .Select(d => new
               {
                   TenantName = SqlFunc.Subqueryable<Tenant>().Where(s => s.Id == d.TenantId).Select(s => s.Name),
                   d.Id,
                   d.CreateTime,
                   d.Application,
                   d.Browser,
                   d.IP,
                   d.Address,
                   UserName = SqlFunc.Subqueryable<User>().Where(s => s.Id == d.UserId).Select(s => s.Name),
                   d.Callsite,
                   d.Message
               }
               ).ToPageAsync(query.Page, query.Limit);
            return new ApiResult(data: new { count = res.TotalItems, items = res.Items });
        }

        [HttpGet, Authority]
        public async Task<ApiResult> Detail(int id)
        {
            var res = await _logRepository.GetModelAsync(d => d.Id == id);
            return new ApiResult(data: res);
        }
    }
}
