﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Common;
using ShenNius.Domain;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Dtos.Input.Sys;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Domain.Repository.Sys;
using ShenNius.Domain.ValueObjects.Enum;
using ShenNius.Infrastructure.Attributes;
using ShenNius.Repository;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ShenNius.Admin.API.Controllers
{
    public class MenuController : ApiControllerBase
    {
        private readonly IMenuRepository _menuRepository;
        private readonly IBaseRepository<Config> _configRepository;
        private readonly IBaseRepository<R_Role_Menu> _r_Role_MenuRepository;
        private readonly IShenNiusContext _sc;
        private readonly IRecycleRepository _recycleRepository;

        public MenuController(IMenuRepository menuRepository, IBaseRepository<Config> configRepository, IBaseRepository<R_Role_Menu> r_Role_MenuRepository, IShenNiusContext sc, IRecycleRepository recycleRepository)
        {
            _menuRepository = menuRepository;
            _configRepository = configRepository;
            _r_Role_MenuRepository = r_Role_MenuRepository;
            _sc = sc;
            _recycleRepository = recycleRepository;
        }
        /// <summary>
        /// 读取菜单配置表的所有按钮信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetBtnCodeList()
        {
            return new ApiResult(await _configRepository.GetListAsync(d => d.Type == nameof(ConfigEnum.Button) && !d.IsDeleted));
        }

        [HttpDelete, Authority]
        public async Task<ApiResult> Deletes([FromBody] DeletesInput input)
        {
            foreach (var item in input.Ids)
            {
                await _menuRepository.UpdateAsync(d => new Menu { DeleteTime = DateTime.Now, IsDeleted = true }, d => d.Id == item && !d.IsDeleted);
            }
            return new ApiResult();
        }

        [HttpDelete, Authority(Action = nameof(BtnEnum.Recycle))]
        public virtual Task<ApiResult> SoftDelete([FromBody] DeletesInput input)
        {
            return _recycleRepository.SoftDeleteAsync(input, _menuRepository);
        }

        [HttpGet, Authority]
        public async Task<ApiResult> GetListPages([FromQuery] KeyListQuery query)
        {
            return await _menuRepository.GetListPagesAsync(query);
        }
        /// <summary>
        /// 获取菜单按钮
        /// </summary>
        /// <param name="menuId"></param>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> BtnCode(int menuId = 0, int roleId = 0)
        {
            return await _menuRepository.BtnCodeByMenuIdAsync(menuId, roleId);
        }
        /// <summary>
        /// 树形菜单
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> TreeByRole(int roleId)
        {
            return await _menuRepository.TreeRoleIdAsync(roleId);
        }
        /// <summary>
        /// 菜单按钮授权
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> SetBtnPermissions([FromBody] RoleMenuBtnInput input)
        {
            //根据角色和菜单查询内容
            var model = await _r_Role_MenuRepository.GetModelAsync(d => d.RoleId == input.RoleId && d.MenuId == input.MenuId && !d.IsDeleted);
            if (model.Id <= 0)
            {
                throw new ArgumentException("您还没有授权当前菜单功能模块");
            }
            if (model.BtnCodeIds != null)
            {
                //判断授权还是取消
                var list = model.BtnCodeIds.ToList();
                if (input.Status)
                {
                    //不包含则添加。包含放任不管
                    if (!list.Contains(input.BtnCodeId))
                    {
                        list.Add(input.BtnCodeId);
                    }
                }
                else
                {
                    //授权 包含则移除
                    if (list.Contains(input.BtnCodeId))
                    {
                        list.Remove(input.BtnCodeId);
                    }
                }
                model.ChangeBtnCodeIds(list.ToArray());
            }
            else
            {
                string[] arry = new string[] { input.BtnCodeId };
                //增加               
                model.ChangeBtnCodeIds(arry);
            }
            var sign = await _r_Role_MenuRepository.UpdateAsync(model);
            return sign > 0 ? new ApiResult(sign) : new ApiResult("菜单按钮授权失败！");


        }
        /// <summary>
        /// 授权菜单
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Authority(Action = nameof(BtnEnum.Auth))]
        [MultiTenant]
        public async Task<ApiResult> AddPermissions([FromBody] PermissionsInput input)
        {
            var model = await _r_Role_MenuRepository.GetModelAsync(d => d.RoleId == input.RoleId && d.MenuId == input.MenuId && !d.IsDeleted);
            if (model.Id > 0)
            {
                return new ApiResult("已经存在该菜单权限了", 400);
            }
            var addModel = R_Role_Menu.Create(input.RoleId, input.MenuId);
            var sign = await _r_Role_MenuRepository.AddAsync(addModel);
            return new ApiResult(sign);
        }

        [HttpGet, Authority]
        public async Task<ApiResult> Detail(int id)
        {
            if (id == 0)
            {
                throw new ArgumentNullException(nameof(id));
            }
            var res = await _menuRepository.GetModelAsync(d => d.Id == id && !d.IsDeleted);
            return new ApiResult(data: res);
        }
        [HttpPost, Authority]
        public async Task<ApiResult> Add([FromBody] MenuInput menuInput)
        {
            return await _menuRepository.AddToUpdateAsync(menuInput);
        }

        [HttpPut, Authority]
        public async Task<ApiResult> Modify([FromBody] MenuModifyInput menuModifyInput)
        {
            return await _menuRepository.ModifyAsync(menuModifyInput);
        }

        [HttpGet]
        public async Task<ApiResult> GetAllParentMenu()
        {
            return await _menuRepository.GetAllParentMenuAsync();
        }
        /// <summary>
        /// 左侧树形菜单
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> LoadLeftMenuTrees()
        {
            return await _menuRepository.LoadLeftMenuTreesAsync(_sc.Id);
        }
    }
}
