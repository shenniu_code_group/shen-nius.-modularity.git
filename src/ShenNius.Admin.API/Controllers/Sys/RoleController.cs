﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using ShenNius.Common;
using ShenNius.Domain;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Dtos.Input.Sys;
using ShenNius.Domain.Dtos.Output.Sys;
using ShenNius.Domain.Entity.Sys;
using ShenNius.Domain.Repository.Sys;
using ShenNius.Domain.ValueObjects.Enum;
using ShenNius.Infrastructure.Attributes;
using ShenNius.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ShenNius.Admin.API.Controllers
{
    public class RoleController : ApiControllerBase
    {
        private readonly IBaseRepository<Role> _roleRepository;
        private readonly IBaseRepository<R_User_Role> _r_User_roleRepository;
        private readonly IRecycleRepository _recycleRepository;
        private readonly IShenNiusContext _sc;
        private readonly IMapper _mapper;
        private readonly IBaseRepository<R_Role_Menu> _r_Role_MenuRepository;

        public RoleController(IBaseRepository<Role> roleRepository, IMapper mapper, IBaseRepository<R_Role_Menu> r_Role_MenuRepository, IBaseRepository<R_User_Role> r_User_roleRepository, IRecycleRepository recycleRepository, IShenNiusContext sc)
        {
            _roleRepository = roleRepository;
            _mapper = mapper;
            _r_Role_MenuRepository = r_Role_MenuRepository;
            _r_User_roleRepository = r_User_roleRepository;
            _recycleRepository = recycleRepository;
            _sc = sc;
        }
        [HttpDelete, Authority]
        public async Task<ApiResult> Deletes([FromBody] DeletesInput input)
        {
            return new ApiResult(await _roleRepository.DeleteAsync(input.Ids));
        }

        [HttpDelete, Authority(Action = nameof(BtnEnum.Recycle))]
        public virtual Task<ApiResult> SoftDelete([FromBody] DeletesInput input)
        {
            return _recycleRepository.SoftDeleteAsync(input, _roleRepository);
        }
        [HttpGet, Authority]
        public async Task<ApiResult> GetListPages([FromQuery] KeyListQuery query)
        {
            Expression<Func<Role, bool>> whereExpression = d => !d.IsDeleted;
            if (!string.IsNullOrEmpty(query.Key))
            {
                whereExpression = d => d.Name.Contains(query.Key) && !d.IsDeleted;
            }
            var res = await _roleRepository.GetPagesAsync(query.Page, query.Limit, whereExpression, d => d.Id, false);
            return new ApiResult(data: new { count = res.TotalItems, items = res.Items });
        }
        /// <summary>
        /// 根据用户获取当前已授权或未授权的所有角色
        /// </summary>
        /// <param name="page">当前页</param>
        /// <param name="userId"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetListPagesByUser(int userId, int page = 1, int limit = 15)
        {
            var query = await _roleRepository.GetPagesAsync(page, limit, d => !d.IsDeleted, d => d.Id, true);
            List<RoleOutput> allRole = new List<RoleOutput>();
            var userRoleList = await _r_User_roleRepository.GetListAsync(d => d.UserId == userId && !d.IsDeleted);
            foreach (var item in query.Items)
            {
                var model = userRoleList.FirstOrDefault(d => d.RoleId == item.Id);
                var role = new RoleOutput()
                {
                    Id = item.Id,
                    Name = item.Name,
                    Description = item.Description,
                    Status = model != null
                };
                allRole.Add(role);
            }
            return new ApiResult(data: new { count = allRole.Count, items = allRole });
        }
        [HttpGet, Authority]
        public async Task<ApiResult> Detail(int id)
        {
            var res = await _roleRepository.GetModelAsync(d => d.Id == id && !d.IsDeleted);
            return new ApiResult(data: res);
        }
        [HttpGet]
        public async Task<ApiResult> List()
        {
            var data = await _roleRepository.GetListAsync(d => !d.IsDeleted);
            return new ApiResult(data: data);
        }

        [HttpPost, Authority]
        public async Task<ApiResult> Add([FromBody] RoleInput input)
        {
            //这里允许每个租户设置的角色名称重复，故只验证当前租户的角色名称是否重复
            var model = await _roleRepository.GetModelAsync(d => !d.IsDeleted && d.Name.Equals(input.Name));
            if (model != null && model.Id > 0)
            {
                return new ApiResult($"已经存在[{input.Name}]该角色了");
            }
            var addModel = _mapper.Map<Role>(input);
            return new ApiResult(await _roleRepository.AddAsync(addModel));
        }

        [HttpPost, Authority(Action = nameof(BtnEnum.Auth))]
        public async Task<ApiResult> SetMenu([FromBody] SetRoleMenuInput input)
        {
            var allUserMenus = await _r_Role_MenuRepository.GetListAsync(d => !d.IsDeleted);
            // allUserRoles.Where(d => d.UserId == setUserRoleInput.UserId && setUserRoleInput.RoleIds.Contains(d.RoleId));
            List<R_Role_Menu> list = new List<R_Role_Menu>();
            foreach (var item in input.MenuIds)
            {
                var model = allUserMenus.Where(d => d.RoleId == input.RoleId && d.MenuId == item);
                if (model == null)
                {
                    var r_User_Menu = R_Role_Menu.Create(input.RoleId, item);
                    list.Add(r_User_Menu);
                    //add                    
                }
            }
            var i = await _r_Role_MenuRepository.AddListAsync(list);
            return i > 0 ? new ApiResult() : new ApiResult("设置菜单失败了！");
        }

        [HttpPut, Authority]
        public async Task<ApiResult> Modify([FromBody] RoleModifyInput input)
        {
            var isExistNamemodel = await _roleRepository.GetModelAsync(d => !d.IsDeleted && d.Id != input.Id && d.Name.Equals(input.Name));
            if (isExistNamemodel != null && isExistNamemodel.Id > 0)
            {
                return new ApiResult($"已经存在[{input.Name}]该角色了");
            }
            var model = await _roleRepository.GetModelAsync(d => !d.IsDeleted && d.Id == input.Id);
            if (model == null && model.Id <= 0)
            {
                return new ApiResult($"不存在[{input.Id}]该条数据");
            }
            model.Modify(input.Id, input.Name, input.Description);
            await _roleRepository.UpdateAsync(model, d => new { d.CreateTime, d.DeleteTime, d.IsDeleted });
            return new ApiResult();
        }
    }
}
