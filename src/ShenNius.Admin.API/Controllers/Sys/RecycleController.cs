﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.Common;
using ShenNius.Domain.Dtos.Common;
using ShenNius.Domain.Repository.Sys;
using ShenNius.Infrastructure.Attributes;
using System.Threading.Tasks;

/*************************************
* 类名：RecycleController
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/4/8 19:24:46
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.Admin.API.Controllers
{
    /// <summary>
    /// 回收站
    /// </summary>
    public class RecycleController : ApiControllerBase
    {
        private readonly IRecycleRepository _recycleRepository;
        public RecycleController(IRecycleRepository recycleRepository)
        {
            _recycleRepository = recycleRepository;
        }
        /// <summary>
        /// 彻底删除
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpDelete, Authority, MultiTenant]

        public Task<ApiResult> Deletes([FromBody] DeletesTenantInput input)
        {
            return _recycleRepository.RealyDeleteAsync(input);
        }

        [HttpGet, Authority, MultiTenant]
        public Task<ApiResult> GetListPages([FromQuery] KeyListTenantQuery query)
        {
            return _recycleRepository.GetPagesAsync(query);
        }

        /// <summary>
        /// 数据还原
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost, Authority]
        public Task<ApiResult> Restore([FromBody] DeletesTenantInput input)
        {
            return _recycleRepository.RestoreAsync(input);
        }
    }
}