﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ShenNius.Admin.API.Jwt;
using ShenNius.Admin.API.Jwt.Extension;
using ShenNius.Admin.API.TimedTask;
using ShenNius.Common.Extension;
using ShenNius.Common.Hepler;
using ShenNius.Domain;
using ShenNius.Domain.Repository.Shop;
using ShenNius.Domain.Repository.Sys;
using ShenNius.FileManagement;
using ShenNius.Infrastructure;
using ShenNius.Infrastructure.Attributes;
using ShenNius.Infrastructure.Hubs;
using ShenNius.Infrastructure.Repository.Shop;
using ShenNius.Infrastructure.Repository.Sys;
using ShenNius.ModuleCore;
using ShenNius.ModuleCore.Context;
using ShenNius.ModuleCore.Extensions;
using ShenNius.Repository;
using SqlSugar;
using System;
using System.Text;

namespace ShenNius.Admin.API
{
    public class ShenniusAdminApiModule:AppModule
    {
        private bool jwtEnable = false;
        public override void OnConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpContextAccessor();
            context.Services.AddSignalR();
            context.Services.AddSqlsugarSetup(option =>
            {
                option.ConnectionString = context.Configuration["ConnectionStrings:MySql"];
                option.DbType = DbType.MySql;
                option.IsAutoCloseConnection = true;
                option.InitKeyType = InitKeyType.Attribute;//从特性读取主键自增信息
            });
            context.Services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            context.Services.AddAutoMapper(typeof(AutomapperProfile));
            var value = context.Configuration["JwtSetting:IsEnable"];
             jwtEnable = !string.IsNullOrEmpty(value) ? Convert.ToBoolean(value) : false;
            Action<MvcOptions> action = options =>
            {
                options.Filters.Add<ModelValidateAttribute>();
                options.Filters.Add(typeof(GlobalExceptionFilter));
            };
            IMvcBuilder mvcBuilder;
            if (jwtEnable)
            {
                // 跨域配置
                context.Services.AddCors(options =>
                {
                    options.AddDefaultPolicy(p => p.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
                });
                context.Services.AddSwaggerSetup();
                //注入MiniProfiler
                // services.AddMiniProfiler(options =>
                //     options.RouteBasePath = "/profiler"
                //);
                context.Services.AddAuthorizationSetup(context.Configuration);
                mvcBuilder = context.Services.AddControllers(action);
            }
            else {
                // 认证
                context.Services.AddSession();
                context.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                 .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, o =>
                 {
                     o.Cookie.Name = "ShenNius.Admin.Mvc";
                     o.LoginPath = new PathString("/sys/user/login");
                     o.LogoutPath = new PathString("/sys/user/Logout");
                     o.Cookie.HttpOnly = true;
                 });
                mvcBuilder = context.Services.AddControllersWithViews(action);
            }
            mvcBuilder.AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.StringEscapeHandling = StringEscapeHandling.EscapeHtml;
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });
            context.Services.AddRouting(options =>
            {
                // 设置URL为小写
                 options.LowercaseUrls = true;
                // 在生成的URL后面添加斜杠
                //options.AppendTrailingSlash = true;
                options.LowercaseQueryStrings = true;
            });
            //，[ApiController]属性作为选择加入到Web API特定约定和行为的方式，这些行为中包含了model验证错误时，自动响应400。
            //导致我的请求总是在ModelState验证通过之后才进入我自定义的ValidateModelAttribute，有没有什么办法能在自动验证之前自定义验证
            context.Services.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true);
            context.Services.AddScoped<IMenuRepository, MenuRepository>();
            context.Services.AddScoped<IRecycleRepository, RecycleRepository>();
            context.Services.AddScoped<IShenNiusContext, ShenNiusContext>();
            context.Services.AddScoped<JwtHelper>();
            context.Services.AddScoped<UserLoginNotifiHub>();
            context.Services.AddHostedService<TimedBackgroundService>();
            MyHttpContext.ServiceProvider = context.Services.BuildServiceProvider();

            context.Services.AddFileUpload(option =>
            {
                option.FileType = FileType.Local;
            });
            context.Services.AddDistributedMemoryCache();
            //shop
            context.Services.AddScoped<IGoodsRepository, GoodsRepository>();
            context.Services.AddScoped<IOrderRepository, OrderRepository>();
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            //避免日志中的中文输出乱码
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            var app = context.GetApplicationBuilder();
            //加入健康检查中间件
            // app.UseHealthChecks("/health");
            NLog.LogManager.LoadConfiguration("nlog.config").GetCurrentClassLogger();
            NLog.LogManager.Configuration.Variables["connectionString"] = context.Configuration["ConnectionStrings:MySql"];
            // 转发将标头代理到当前请求，配合 Nginx 使用，获取用户真实IP
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
            app.UseHsts();
            app.UseHttpsRedirection();
            app.UseRouting();
            if (!jwtEnable)
            {
                app.UseAuthentication();
            }
            app.UseAuthorization();
            if (jwtEnable)
            {
                // 跨域
                app.UseCors(p => p.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
                app.UseSwaggerMiddle();
                // 路由映射
                app.UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers().RequireAuthorization();
                    //全局路由配置
                    endpoints.MapControllerRoute(
                         name: "default",
                           pattern: "{controller=Home}/{action=Index}/{id?}"
                        );
                    //这里要说下，为啥地址要写 /api/xxx 
                    //因为我前后端分离了，而且使用的是代理模式，所以如果你不用/api/xxx的这个规则的话，会出现跨域问题，毕竟这个不是我的controller的路由，而且自己定义的路由
                    endpoints.MapHub<UserLoginNotifiHub>("userLoginNotifiHub");
                });
            }
            else
            {
                app.UseSession();
                app.UseStaticFiles();
                app.UseStatusCodePagesWithReExecute("/error.html");
                // 路由映射
                app.UseEndpoints(endpoints =>
                {
                    //这个扩展方法全局添加也可以代替Authorize,如果因重写了IAuthorizationFilter就可以不添加。
                    endpoints?.MapControllers().RequireAuthorization();
                    endpoints.MapControllerRoute(
                    name: "MyArea",
                    pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                    //全局路由配置
                    endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=home}/{action=index}/{id?}");
                    endpoints.MapHub<UserLoginNotifiHub>("userLoginNotifiHub");
                });
            }
        }
    }
}
