﻿using System;

namespace ShenNius.OfficialWeb.Core.Common
{
    public static class StaticExtension
    {
        public static string ToWebString(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string TrimEnd(this string sourceStr, string removeStr)
        {
            if (string.IsNullOrEmpty(sourceStr) || !sourceStr.EndsWith(removeStr))
            {
                return sourceStr;
            }
            return sourceStr.Substring(0, sourceStr.Length - removeStr.Length);
        }
    }
}
