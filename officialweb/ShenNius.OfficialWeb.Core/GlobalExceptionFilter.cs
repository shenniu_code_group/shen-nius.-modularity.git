﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using ShenNius.OfficialWeb.Core.Common;
using System.Net;

namespace ShenNius.OfficialWeb.Core
{
    /// <summary>
    /// Http全局异常过滤器
    /// </summary>
    public class GlobalExceptionFilter : IExceptionFilter
    {
        private readonly ILogger<GlobalExceptionFilter> _logger;

        public GlobalExceptionFilter(ILogger<GlobalExceptionFilter> logger)
        {
            _logger = logger;
        }
        public void OnException(ExceptionContext context)
        {
            string msg = string.Empty;           
            switch (context.HttpContext.Response.StatusCode)
            {
                case (int)HttpStatusCode.Forbidden:
                    msg = "服务器拒绝满足需求";
                    break;
                case (int)HttpStatusCode.NotFound:
                    msg = "未找到服务";
                    break;
                case (int)HttpStatusCode.BadGateway:
                    msg = "请求错误";
                    break;
                case (int)HttpStatusCode.Unauthorized:
                    msg = "未授权";
                    break;
                case (int)HttpStatusCode.InternalServerError:
                    msg = "内部错误";
                    break;
                default:                   
                    break;
            }
            try
            {
                msg =$"{msg}:{context.Exception.StackTrace}" ;
                _logger.LogError(msg);
                WebHelper.WriteLog(msg);
            }
            catch
            {
            }            
            return;
        }
    }
}
