﻿using ShenNius.OfficialWeb.Core.Domain.Entity;
using ShenNius.OfficialWeb.Core.Dtos.Output;
using ShenNius.Repository.Extensions;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ShenNius.OfficialWeb.Core.Domain.Services
{
    public class CmsDomainService
    {
        private readonly ISqlSugarClient _db;
        public CmsDomainService(ISqlSugarClient db)
        {
            _db = db;
        }
        public async Task<bool> IsExistUrl(int siteId, string parentColumnUrl, string childColumnUrl)
        {
            bool sign;
            if (string.IsNullOrEmpty(childColumnUrl))
            {
                sign = await IsExistColumnUrl(siteId,parentColumnUrl);
            }
            else
            {
                sign = await IsExistColumnUrl(siteId,parentColumnUrl, childColumnUrl);
            }
            return sign;
        }
        private async Task<bool> IsExistColumnUrl(int siteId,params string[] urls)
        {
            if (urls.Length < 0)
            {
                return false;
            }
            var list = await GetColumnAsync(siteId);
            Column model = null;
            foreach (var item in urls)
            {
                if (string.IsNullOrEmpty(item))
                {
                    return false;
                }
                model = list.Where(d => d.EnTitle.Equals(item)).FirstOrDefault();
                if (model == null)
                {
                    return false;
                }
                else
                {
                    continue;
                }
            }
            return true;
        }

        public async Task<List<Column>> GetColumnAsync(int siteId)
        {
            return await _db.Queryable<Column>().Where(d => !d.IsDeleted && d.TenantId == siteId).ToListAsync();
        }

        public async Task<List<string>> GetAllTagsAsync(string spell, int siteId)
        {
            // 进来可能是大类或子类，1、大类下面有子类，2、大类下面没有子类 3、进来的是子类
            if (string.IsNullOrEmpty(spell))
            {
                throw new ArgumentNullException("栏目url不能为空");
            }
            var model = (await GetColumnAsync(siteId)).Where(d => d.EnTitle.Equals(spell)).FirstOrDefault();
            List<int> columnListId = new List<int>();
            if (model?.ParentId == 0)
            {
                //查下该大类有没有子类
                columnListId = (await GetColumnAsync(siteId)).Where(d => d.ParentId == model.Id).Select(d => d.Id).ToList();
                //没有子类直接赋值大类id
                if (columnListId.Count == 0)
                {
                    columnListId.Add(model.Id);
                }
            }
            else
            {
                //进来了子类
                if (model?.Id!=null)
                {
                    columnListId.Add(model.Id);
                }               
            }
            var list = await _db.Queryable<Article>().Where(d => d.Audit == true && columnListId.Contains(d.ColumnId)).GroupBy(d => d.Tag).Select(d => d.Tag).ToListAsync();
            return list;
        }

        /// <summary>
        /// 前台控制器直接调用文章列表
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="parentColumnSpell"></param>
        /// <param name="childColumnSpell"></param>
        /// <param name="keyword"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public async Task<ArticleListOutput> GetArticleListAsync(int siteId, string parentColumnSpell, string childColumnSpell, string keyword, int page = 1)
        {
            Column columnModel = null;
            List<int> allChildColumnIdList = new List<int>();
            var columnList = await GetColumnAsync(siteId);
            //keyword
            Expression<Func<Article, Column, bool>> expression = (ca, cc) => ca.TenantId == siteId;
            if (!string.IsNullOrEmpty(keyword))
            {
                keyword = HttpUtility.UrlDecode(keyword);
                expression = (ca, cc) => ca.KeyWord.Contains(keyword) || ca.Tag.Contains(keyword);
            }
            //先判断大类url是否为空
            if (!string.IsNullOrEmpty(parentColumnSpell))
            {
                var parentColumnModel = columnList.Where(d => d.ParentId == 0 && d.EnTitle.Equals(parentColumnSpell.Trim())).FirstOrDefault();
                if (parentColumnModel != null)
                {
                    columnModel = parentColumnModel;
                    allChildColumnIdList = columnList.Where(d => d.ParentId == parentColumnModel.Id && d.ParentId != 0).Select(d => d.Id).ToList();
                    //如果这个大栏目没有子栏目时就根据大栏目去查找对应的文章
                    if (allChildColumnIdList.Count == 0)
                    {
                        expression = (ca, cc) => ca.ColumnId == columnModel.Id;
                    }
                }
            }
            //判断allChildColumnIdList是否有值
            if (allChildColumnIdList.Count > 0 && allChildColumnIdList.Any())
            {
                expression = (ca, cc) => allChildColumnIdList.Contains(ca.ColumnId);
            }
            //判断子类url是否为空
            if (!string.IsNullOrEmpty(childColumnSpell))
            {
                var childColumnModel = columnList.Where(d => d.ParentId > 0 && d.EnTitle.Equals(childColumnSpell.Trim())).FirstOrDefault();
                if (childColumnModel != null)
                {
                    columnModel = childColumnModel;
                    expression = (ca, cc) => ca.ColumnId == childColumnModel.Id;
                }
            }
            var query = await GetArtcileByConditionAsync(expression, page, 15);
            var data =new ArticleListOutput(query.Items, query.TotalItems, columnModel);
            return  data;
        }

        /// <summary>
        /// 前端控制台直接调用文章详情
        /// </summary>
        /// <param name="id"></param>
        /// <param name="siteId"></param>
        /// <param name="parentColumnSpell"></param>
        /// <param name="childColumnspell"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        public  async Task<ArticleDetailOutput> GetArtcileDetailAsync(int id, int siteId, string parentColumnSpell = null, string childColumnspell = null)
        {
            List<Column> columnList = await GetColumnAsync(siteId);
            if (string.IsNullOrEmpty(parentColumnSpell) && string.IsNullOrEmpty(childColumnspell))
            {
                throw new ArgumentNullException("栏目不能为空");
            }
            string columnUrl = string.IsNullOrEmpty(childColumnspell) ? parentColumnSpell : childColumnspell;
            var model = await GetArtcileDetailAsync((ca, cc) => ca.Id == id && ca.Audit == true && cc.EnTitle.Trim() == columnUrl.Trim());
           
            var upArticle = await GetNextOrUpArticleAsync((ca, cc) => ca.Id > id && ca.ColumnId == model.ColumnId && ca.TenantId == siteId);
            var nextArticle = await GetNextOrUpArticleAsync((ca, cc) => ca.Id < id && ca.ColumnId == model.ColumnId && ca.TenantId == siteId);
            var sameColumnArticle = await GetArtcileByConditionAsync((ca, cc) => ca.ColumnId == model.ColumnId && ca.Audit == true && ca.TenantId == siteId, 1, 6);

            var detailModel = new ArticleDetailOutput
            (
                 model,
                upArticle,
                nextArticle,
                 sameColumnArticle.Items
            );
            return detailModel;
        }

        /// <summary>
        /// 文章详情
        /// </summary>
        /// <param name="where">查询条件</param>
        /// <returns></returns>
        internal async Task<ArticleOutput> GetArtcileDetailAsync(Expression<Func<Article, Column, bool>> where)
        {
            return await _db.Queryable<Article, Column>((ca, cc) => new object[] { JoinType.Inner, ca.ColumnId == cc.Id })
                   .WhereIF(where != null, where)
                  .Select((ca, cc) => new ArticleOutput
                  {
                      Title = ca.Title,
                      Id = ca.Id,
                      ColumnId = ca.ColumnId,
                      Summary = ca.Summary,
                      EnTitle = cc.EnTitle,
                      Author = ca.Author,
                      Source = ca.Source,
                      Tag = ca.Tag,
                      CreateTime = ca.CreateTime,
                      Content = ca.Content,
                      ThumImg = ca.ThumImg,
                      ColumnName = cc.Title,
                      KeyWord = ca.KeyWord,
                      ParentColumnUrl = SqlFunc.Subqueryable<Column>().Where(s => s.Id == cc.ParentId).Select(s => s.EnTitle)
                  })
                  .FirstAsync()??new ArticleOutput();
        }
        /// <summary>
        /// 根据条件查询文章
        /// </summary>
        /// <param name="where"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<Page<ArticleOutput>> GetArtcileByConditionAsync(Expression<Func<Article, Column, bool>> where, int pageIndex, int pageSize)
        {
            return await _db.Queryable<Article, Column>((ca, cc) => new object[] { JoinType.Inner, ca.ColumnId == cc.Id })
                  .WhereIF(where != null, where)
                  .OrderBy((ca, cc) => ca.Id, OrderByType.Desc)
                  .Select((ca, cc) => new ArticleOutput
                  {
                      Title = ca.Title,
                      Id = ca.Id,
                      ColumnId = ca.ColumnId,
                      Summary = ca.Summary,
                      EnTitle = cc.EnTitle,
                      Author = ca.Author,
                      Source = ca.Source,
                      Tag = ca.Tag,
                      CreateTime = ca.CreateTime,
                      ColumnName = cc.Title,
                      Content = ca.Content,
                      ThumImg = ca.ThumImg,
                      IsTop = ca.IsTop,
                      IsHot = ca.IsHot,
                      ParentColumnUrl = SqlFunc.Subqueryable<Column>().Where(s => s.Id == cc.ParentId).Select(s => s.EnTitle)
                  })
                  .ToPageAsync(pageIndex, pageSize);
        }
        /// <summary>
        /// 文章详情页下列的 上一篇和下一篇
        /// </summary>
        /// <param name="expression">查询条件</param>
        /// <returns></returns>
        public async Task<ArticleOutput> GetNextOrUpArticleAsync(Expression<Func<Article, Column, bool>> expression)
        {
            //(ca, cc) => ca.Id < id && ca.ColumnId == columnId
           return  await _db.Queryable<Article, Column>((ca, cc) => new object[] { JoinType.Inner, ca.ColumnId == cc.Id })
                .Where(expression)
                .OrderBy((ca, cc) => ca.Id, OrderByType.Desc)
                .Select((ca, cc) => new ArticleOutput
                {
                    Title = ca.Title,
                    Id = ca.Id,
                    ColumnId = ca.ColumnId,
                    EnTitle = cc.EnTitle,
                    ParentColumnUrl = SqlFunc.Subqueryable<Column>().Where(s => s.Id == cc.ParentId).Select(s => s.EnTitle)
                })
                .FirstAsync()??new ArticleOutput();
        }
    }
}
