﻿using SqlSugar;

namespace ShenNius.OfficialWeb.Core.Domain.Entity
{
    [SugarTable("Cms_Article")]
    public class Article : BaseTenantEntity
    {
        /// <summary>
        /// Desc:栏目ID
        /// Default:0
        /// Nullable:False
        /// </summary>
        public int ColumnId { get; private set; }
        [SugarColumn(IsIgnore = true)]
        public string ColumnName { get; private set; }
        /// <summary>
        /// Desc:文章标题
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Desc:文章标题颜色
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string TitleColor { get; private set; }

        /// <summary>
        /// Desc:文章副标题
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string SubTitle { get; private set; }

        /// <summary>
        /// Desc:作者
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Author { get; private set; }

        /// <summary>
        /// Desc:来源
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Source { get; private set; }

        /// <summary>
        /// Desc:是否外链
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsLink { get; private set; }

        /// <summary>
        /// Desc:外部链接地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string LinkUrl { get; private set; }

        /// <summary>
        /// Desc:文章标签
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Tag { get; private set; }

        /// <summary>
        /// Desc:文章图
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string ThumImg { get; private set; }

        /// <summary>
        /// Desc:视频链接地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string VideoUrl { get; private set; }

        /// <summary>
        /// Desc:是否置顶
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsTop { get; private set; } 

        /// <summary>
        /// Desc:是否热点
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsHot { get; private set; } 

        /// <summary>
        /// Desc:是否允许评论
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsComment { get; private set; } 
        /// <summary>
        /// Desc:是否在回收站
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsRecycle { get; private set; } 

        /// <summary>
        /// Desc:审核状态
        /// Default:b'1'
        /// Nullable:False
        /// </summary>
        public bool Audit { get; private set; } 

        /// <summary>
        /// SEO关键字
        /// </summary>
        public string KeyWord { get; private set; }

        /// <summary>
        /// 文章摘要
        /// </summary>
        public string Summary { get; private set; }

        /// <summary>
        /// 文章内容
        /// </summary>
        public string Content { get; private set; }



    }
}