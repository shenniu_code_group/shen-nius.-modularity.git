﻿using SqlSugar;

/*************************************
* 类名：Column
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/3/11 16:57:35
*┌───────────────────────────────────┐　    
*│　      版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.OfficialWeb.Core.Domain.Entity
{
    [SugarTable("Cms_Column")]
    public class Column : BaseTenantEntity
    {
        /// <summary>
        /// Desc:栏目标题
        /// Default:-
        /// Nullable:False
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Desc:英文栏位名称
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string EnTitle { get; private set; }

        /// <summary>
        /// Desc:栏位副标题
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string SubTitle { get; private set; }

        /// <summary>
        /// Desc:栏位属性
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Attr { get; private set; }

        /// <summary>
        /// Desc:栏位图片
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string ImgUrl { get; private set; }

        /// <summary>
        /// Desc:关键词
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Keyword { get; private set; }

        /// <summary>
        /// Desc:描述
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Summary { get; private set; }

        // Desc:栏位集合    
        public string ParentList { get; protected set; }
        /// Desc:栏位等级     
        public int Layer { get; protected set; }
        public int ParentId { get; protected set; }
    }
}