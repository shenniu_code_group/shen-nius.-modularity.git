﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShenNius.OfficialWeb.Core.Domain.Entity
{
    public class BaseTenantEntity
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public int Id { get; protected set; }
        public int TenantId { get;private set; }
        public DateTime CreateTime { get; protected set; } 
        public bool IsDeleted { get; private set; }


    }
}
