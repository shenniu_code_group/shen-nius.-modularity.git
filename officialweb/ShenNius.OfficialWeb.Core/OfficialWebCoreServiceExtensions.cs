﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ShenNius.OfficialWeb.Core.Common;
using ShenNius.OfficialWeb.Core.Domain.Services;
using ShenNius.Repository;
using SqlSugar;
using System.Text;
using System.Text.Encodings.Web;
using System.Text.Unicode;

namespace ShenNius.OfficialWeb.Core
{
    public static class OfficialWebCoreServiceExtensions
    {
        public static void AddOfficialWebCore(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSqlsugarSetup(option =>
            {
                option.ConnectionString = configuration["ConnectionStrings:MySql"];
                option.DbType = DbType.MySql;
                option.IsAutoCloseConnection = true;
                option.InitKeyType = InitKeyType.Attribute;//从特性读取主键自增信息
            });
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<CmsDomainService>();
            services.AddDistributedMemoryCache();
            //解决中文被编码
            services.AddSingleton(HtmlEncoder.Create(UnicodeRanges.All));
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance); 
        }
    }
}
