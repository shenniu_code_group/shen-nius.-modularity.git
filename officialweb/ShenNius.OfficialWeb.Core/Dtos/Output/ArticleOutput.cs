﻿using ShenNius.OfficialWeb.Core.Domain.Entity;
using System;

/*************************************
* 类名：ArticleOutput
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/4/2 17:24:48
*┌───────────────────────────────────┐　    
*│　     版权所有：神牛软件　　　　	 │
*└───────────────────────────────────┘
**************************************/

namespace ShenNius.OfficialWeb.Core.Dtos.Output
{
    public class ArticleOutput
    {
        public int Id { get; set; }
        public string Summary { get; set; }
        public int ColumnId { get; set; } = 0;
        public string Title { get; set; }
        /// <summary>
        /// Desc:作者
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Desc:来源
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Desc:是否外链
        /// Default:b'0'
        /// Nullable:False
        /// </summary>
        public bool IsLink { get; set; } = false;

        /// <summary>
        /// Desc:外部链接地址
        /// Default:-
        /// Nullable:True
        /// </summary>
        public string LinkUrl { get; set; }
        public DateTime CreateTime { get; set; }
        public string Tag { get; set; }
        public string EnTitle { get; set; }
        public string ThumImg { get; set; }
        public string Content { get; set; }
        public string ColumnName { get; set; }
        /// <summary>
        /// Desc:点击量
        /// Default:0
        /// Nullable:False
        /// </summary>
        public int Hits { get; set; } = 0;
        public string KeyWord { get; set; }
        public bool IsTop { get; set; }
        /// <summary>
        /// Desc:是否热点
        /// </summary>
        public bool IsHot { get; set; }
        public string ParentColumnUrl { get; set; }
    }
    /// <summary>
    /// 文章详情页返回实体
    /// </summary>
    public class ArticleDetailOutput
    {
        public ArticleDetailOutput(ArticleOutput detail, ArticleOutput upArticle, ArticleOutput nextArticle, List<ArticleOutput> sameColumnArticle)
        {
            Detail = detail != null ? detail : new ArticleOutput();
            UpArticle = upArticle != null ? upArticle : new ArticleOutput();
            NextArticle = nextArticle != null ? nextArticle : new ArticleOutput();
            SameColumnArticle = sameColumnArticle != null? sameColumnArticle : new List<ArticleOutput>();
        }
        public ArticleOutput Detail { get;private set; } 
        public ArticleOutput UpArticle { get; private set; } 
        public ArticleOutput NextArticle { get; private set; } 
        public List< ArticleOutput> SameColumnArticle { get; private set; } 
    }
    public class ArticleListOutput
    {
        public ArticleListOutput(List<ArticleOutput> list, long total, Column column)
        {
            List = list != null ? list : new List<ArticleOutput>();
            Total =  total==0? 0 : total;
            Column = column != null ? column : new Column();

        }
        /// <summary>
        /// 文章列表
        /// </summary>
        public List<ArticleOutput> List { get;private set; }
        public long Total { get; private set; }
        public Column Column { get; private set; }      
    }
}