﻿using Microsoft.AspNetCore.Mvc;
using ShenNius.OfficialWeb.Core.Domain.Entity;
using ShenNius.OfficialWeb.Core.Domain.Enum;
using ShenNius.OfficialWeb.Core.Domain.Services;
using ShenNius.OfficialWeb.Core.Dtos.Input;
using ShenNius.Repository;

namespace ShenNius.OfficialWeb.Core.Controllers
{
    /// <summary>
    /// 内容管理控制器
    /// </summary>
    [Route("api/app/[controller]/[action]")]
    [ApiController]
    public class CmsController : ControllerBase
    {
        private readonly IBaseRepository<AdvList> _advlistRepository;
        private readonly IBaseRepository<Message> _messageRepository;
        private readonly CmsDomainService _cmsDomainService;
        private readonly IBaseRepository<Tenant> _tenantRepository;
        public CmsController(IBaseRepository<AdvList> advlistRepository,
 IBaseRepository<Tenant> tenantRepository, IBaseRepository<Message> messageRepository, CmsDomainService cmsDomainService)
        {
            _advlistRepository = advlistRepository;
            _messageRepository = messageRepository;
            _cmsDomainService = cmsDomainService;
            _tenantRepository = tenantRepository;
        }


        [HttpGet]
        public async Task<ApiResult> GetAllColumn(int siteId)
        {
            var data = await _cmsDomainService.GetColumnAsync(siteId);
            return new ApiResult(data);
        }
        /// <summary>
        /// 请求站点信息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetSiteInfo(int siteId)
        {
            var model = await _tenantRepository.GetModelAsync(d => d.Id.Equals(siteId));
            if (model == null)
            {
                throw new ArgumentNullException("请求的站点信息为空");
            }
            return new ApiResult(model);
        }

        [HttpGet]
        public async Task<ApiResult> GetIndex(int siteId)
        {
            var recArticleList = await _cmsDomainService.GetArtcileByConditionAsync((ca, cc) => ca.IsTop == true && ca.Audit == true && ca.TenantId == siteId, 1, 6);
            var categoryArticleList = await _cmsDomainService.GetArtcileByConditionAsync((ca, cc) => ca.Audit == true && ca.TenantId == siteId, 1, 12);
            return new ApiResult(data: new
            {
                recArticleList = recArticleList.Items,
                categoryArticleList = categoryArticleList.Items
            });
        }
        [HttpGet]
        public async Task<ApiResult> GetAllTags(string spell, int siteId)
        {
            var list = await _cmsDomainService.GetAllTagsAsync(spell, siteId);
            return new ApiResult(list);
        }
        /// <summary>
        /// 根据ID查询案例/新闻详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetDetail(int id, int siteId, string parentColumnSpell = null, string childColumnspell = null)
        {
            var model = await _cmsDomainService.GetArtcileDetailAsync(id, siteId, parentColumnSpell, childColumnspell);
            return new ApiResult(model);
        }
        /// <summary>
        /// 文章列表
        /// </summary>
        /// <param name="parentColumnSpell"></param>
        /// <param name="childColumnSpell"></param>
        /// <param name="keyword"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult> GetList(int siteId, string parentColumnSpell, string childColumnSpell, string keyword, int page = 1)
        {
            var data = await _cmsDomainService.GetArticleListAsync(siteId, parentColumnSpell, childColumnSpell, keyword, page);
            return new ApiResult(data);
        }
        /// <summary>
        ///  用户留言，提交需求
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> AddMsg([FromBody] MessageInput messageInput)
        {
            messageInput.IP = HttpContext.Request.Headers["X-Forwarded-For"].FirstOrDefault() ?? HttpContext.Connection.RemoteIpAddress.ToString();

            var list = await _messageRepository.GetListAsync(m => m.IP == messageInput.IP && m.TenantId == messageInput.TenantId && m.Types == messageInput.Types, m => m.CreateTime, true);
            if (list.Count() > 3)
            {
                throw new ArgumentNullException("您提交的次数过多，请稍后重试！~");
            }
            // messageInput.Address = IpParseHelper.GetAddressByIP(messageInput.IP);
            // var model = _mapper.Map<Message>(messageInput);
            Message model = new Message();
            await _messageRepository.AddAsync(model);
            return new ApiResult();
        }
        [HttpGet]
        public async Task<ApiResult> LoadMessage(int businessId, int siteId, string types)
        {
            var result = await _messageRepository.GetListAsync(x => x.BusinessId == businessId && x.TenantId.Equals(siteId) && x.Types.Equals(types), x => x.CreateTime, false);
            return ApiResult.Successed(result);
        }
        [HttpGet]
        public async Task<ApiResult> GetAdvList(int siteId, AdvEnum type)
        {
            var advList = await _advlistRepository.GetListAsync(x => x.IsDeleted == true && x.TenantId == siteId && x.Type == type, x => x.Sort, false);
            return ApiResult.Successed(advList);
        }

    }
}