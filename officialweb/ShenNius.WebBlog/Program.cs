using Microsoft.AspNetCore.Rewrite;
using Microsoft.Net.Http.Headers;
using ShenNius.OfficialWeb.Core;
using ShenNius.OfficialWeb.Core.Common;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.
builder.Services.AddControllersWithViews(options =>
{
    options.Filters.Add(typeof(GlobalExceptionFilter));
});
builder.Services.AddOfficialWebCore(builder.Configuration);
builder.Services.AddHttpContextAccessor();
builder.Services.Configure<DomainConfig>(builder.Configuration.GetSection(nameof(DomainConfig)));
builder.Services.AddResponseCompression();
builder.Services.AddRouting(options => options.LowercaseUrls = true);
var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    app.UseHsts();
}
app.UseResponseCompression();
//��̬�ļ�����(css,js)
app.UseStaticFiles(
    new StaticFileOptions
    {
        OnPrepareResponse = ctx =>
        {
            const int durationInSeconds = 60 * 60 * 24;
            ctx.Context.Response.Headers[HeaderNames.CacheControl] =
                "public,max-age=" + durationInSeconds;
        }
    });
//app.UseHttpsRedirection();
app.UseStaticFiles();
var options = new RewriteOptions();
options.AddRewrite(@"(\w+)/(\w+)/page_(\d+).html", "/Home/NewsList?parentColumnUrl=$1&childColumnUrl=$2&page=$3", true);
options.AddRewrite(@"(\w+)/page_(\d+).html", "/Home/NewsList?parentColumnUrl=$1&page=$2", true);
//options.AddRewrite(@"(\w+)/(\w+)/(\d+).html", "/Home/Detail?parentColumnUrl=$1&childColumnUrl=$2&id=$3", true);
//options.AddRewrite(@"(\w+)/(\d+).html", "/Home/Detail?parentColumnUrl=$1&id=$2", true);
options.Add(WebHelper.RedirectXmlFileRequests);
app.UseRewriter(options);
app.UseStatusCodePagesWithReExecute("/404.html");
app.UseRouting();

app.UseEndpoints(endpoints =>
{
    // biancheng/web/54.html
    endpoints.MapControllerRoute("detailParentColumnUrlChild", "{parentColumnUrl}/{id}.html",
new { controller = "Home", action = "Detail", parentColumnUrl= @"(\w+)", id = @"\d+" }
);
    // biancheng/53.html
    endpoints.MapControllerRoute("detailParentColumnUrl", "{parentColumnUrl}/{childColumnUrl}/{id}.html",
new { controller = "Home", action = "Detail", parentColumnUrl = @"(\w+)", childColumnUrl= @"(\w+)", id = @"\d+" }
);
    endpoints.MapControllerRoute("parentColumnUrlChild", "{parentColumnUrl}/{childColumnUrl}/",
new { controller = "Home", action = "NewsList"}
);
    endpoints.MapControllerRoute("parentColumnUrl", "{parentColumnUrl}/",
     new { controller = "Home", action = "NewsList"}
     );
    endpoints.MapControllerRoute(
        name: "default",
        pattern: "{controller=Home}/{action=Index}/{id?}");
});
app.Run();
