﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ShenNius.OfficialWeb.Core.Common;
using ShenNius.OfficialWeb.Core.Domain.Services;
using SqlSugar;
using ShenNius.WebBlog.ViewModels;

namespace ShenNius.Blog.ViewComponents
{
    public class RightViewComponent : ViewComponent
    {      
        private readonly DomainConfig _domainConfig;
        private readonly CmsDomainService _cmsDomainService;
        public RightViewComponent(IOptionsMonitor<DomainConfig> options, CmsDomainService cmsDomainService)
        {
            _domainConfig = options.CurrentValue;
            _cmsDomainService = cmsDomainService;
        }
        public async Task<IViewComponentResult> InvokeAsync(string columnParentSpell = null, bool isIndex = false)
        {
            if (string.IsNullOrWhiteSpace(columnParentSpell))
            {
                columnParentSpell = "blog";
            }
            var monthArticle = await _cmsDomainService.GetArtcileByConditionAsync((ca, cc) => ca.Audit == true && SqlFunc.DateIsSame(DateTime.Now, ca.CreateTime, DateType.Month), 1, 10);
            var currentSpellArticle = await _cmsDomainService.GetArtcileByConditionAsync((ca, cc) => ca.Audit == true && cc.EnTitle.Trim().Equals(columnParentSpell), 1, 10);
            var allChildColumn = (await _cmsDomainService.GetColumnAsync(_domainConfig.SiteId)).Where(d => d.ParentId != 0).ToList();

            var listTags = await _cmsDomainService.GetAllTagsAsync(columnParentSpell, _domainConfig.SiteId);
            RightVM vM = new RightVM(monthArticle.Items, currentSpellArticle.Items, listTags,isIndex);
                    
            return View(vM);
        }
    }
}
