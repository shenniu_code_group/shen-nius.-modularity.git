﻿

//绑定文章父栏目  
function BindParentArticleColumn(value) {
    //加载文章父栏目下拉框
    $.ajax({
        type: "Get",
        url: "/Column/LoadAllColumn",
        dataType: "json",
        data: {},
        success: function (data) {            
            if (data !== null) {
                var select = $("#selectArticleColumn");
                for (var i = 0; i < data.length; i++) {
                    var val = data[i].Id;
                    if (val === value)
                        select.append("<option value='" + val + "' selected>" + data[i].Name + "</option>");  //添加option
                    else
                        select.append("<option value='" + val + "'>" + data[i].Name + "</option>");  //添加option
                }
                layui.form.render('select');
            }
        }
    });
}

//绑定文章子栏目
function BindChildArticleColumn(parentId, value) {
    $("#ColumnChildId").empty();  //初始清空select中的option
    //加载文章子栏目下拉框
    $.ajax({
        type: "Get",
        url: "/Column/LoadChildColumn",
        dataType: "json",
        data: { parentId: parentId },
        success: function (data) {
            var relData = data;
            var select = $("#ColumnChildId");
            if (relData !== null) {
                for (var i = 0; i < relData.length; i++) {
                    var val = relData[i].Id;
                    if (val === value)
                        select.append("<option value='" + val + "' selected>" + relData[i].Name + "</option>");  //添加option
                    else
                        select.append("<option value='" + val + "'>" + relData[i].Name + "</option>");  //添加option
                }
            }

            layui.form.render('select');
        }
    });
}
