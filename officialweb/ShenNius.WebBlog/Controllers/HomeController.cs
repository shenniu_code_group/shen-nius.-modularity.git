﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ShenNius.OfficialWeb.Core.Common;
using ShenNius.OfficialWeb.Core.Domain.Entity;
using ShenNius.OfficialWeb.Core.Domain.Enum;
using ShenNius.OfficialWeb.Core.Domain.Services;
using ShenNius.OfficialWeb.Core.Dtos.Output;
using ShenNius.Repository;
using ShenNius.WebBlog.ViewModels;
using X.PagedList;

namespace ShenNius.WebBlog.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly CmsDomainService _cmsDomainService;
        private readonly IBaseRepository<AdvList> _advlistRepository;
        private readonly DomainConfig _domainConfig;
        private readonly int siteId = 1;
        public HomeController(IOptionsMonitor<DomainConfig> domainConfig, ILogger<HomeController> logger, CmsDomainService cmsDomainService, IBaseRepository<AdvList> advlistRepository)
        {
            _domainConfig = domainConfig.CurrentValue;
            _logger = logger;
            _cmsDomainService = cmsDomainService;
            _advlistRepository = advlistRepository;
            if (_domainConfig.SiteId > 0)
            {
                siteId = _domainConfig.SiteId;
            }
        }

        [HttpGet("/")]
        public async Task<IActionResult> Index()
        {
            var recArticleList = await _cmsDomainService.GetArtcileByConditionAsync((ca, cc) => ca.IsTop == true && ca.Audit == true && ca.TenantId == _domainConfig.SiteId, 1, 6);
            var categoryArticleList = await _cmsDomainService.GetArtcileByConditionAsync((ca, cc) => ca.Audit == true && ca.TenantId == siteId, 1, 12);

            var advList = await _advlistRepository.GetListAsync(x => x.IsDeleted == true && x.TenantId == siteId && x.Type == AdvEnum.FriendlyLink, x => x.Sort, false);
            IndexVM vM = new IndexVM(recArticleList.Items, categoryArticleList.Items, advList);
            return View(vM);
        }
        [HttpGet]
        public async Task<IActionResult> Detail(int id, string parentColumnUrl, string childColumnUrl = null)
        {
            bool sign = await _cmsDomainService.IsExistUrl(siteId, parentColumnUrl, childColumnUrl);
            if (!sign)
                return NotFound();

            var model = await _cmsDomainService.GetArtcileDetailAsync(id, siteId, parentColumnUrl, childColumnUrl);
            if (model?.Detail?.Id == 0)
            {
                return NotFound();
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> NewsList(string parentColumnUrl = "", string childColumnUrl = "", int page = 1, int limit = 15)
        {
            ViewBag.CurrentPage = page;
            string url = string.Empty; string currentColumnUrl = string.Empty;           
            bool sign=await _cmsDomainService.IsExistUrl(siteId,parentColumnUrl, childColumnUrl);
            if (!sign)
            {
                return NotFound();
            }
            if (!string.IsNullOrEmpty(parentColumnUrl))
            {
                url += $"{parentColumnUrl}";
                currentColumnUrl = parentColumnUrl;
            }
            if (!string.IsNullOrEmpty(childColumnUrl))
            {
                url += $"/{childColumnUrl}";
                currentColumnUrl = childColumnUrl;
            }
            var data = await _cmsDomainService.GetArticleListAsync(siteId: siteId, parentColumnUrl, childColumnUrl, "", page);
            if (data == null)
                return NotFound();
            //分页
            ViewBag.PageHtml = WebHelper.ShowPageNavigate(limit, page, (int)data.Total, $"{_domainConfig.Host}/{url}/");
            ViewBag.CurrentColumnUrl = currentColumnUrl;
            return View(data);
        }

        [Route("/about.html")]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }
        [Route("/link.html")]
        public async Task<IActionResult> Link()
        {
            var advList = await _advlistRepository.GetListAsync(x => !x.IsDeleted && x.TenantId == siteId && x.Type == AdvEnum.GoodBlog, x => x.Sort, false);

            return View(advList);
        }
        [HttpGet("/search.html")]
        public async Task<IActionResult> Search(int page = 1, string keyword = null)
        {
            if (string.IsNullOrEmpty(keyword))
            {
                return NotFound();
            }
            var articleList = await _cmsDomainService.GetArticleListAsync(siteId: siteId, null, null, keyword, page);
            if (articleList == null)
                return NotFound();

            TempData["CurrentKeyword"] = keyword;
            ViewData["Page"] = new StaticPagedList<ArticleOutput>(articleList.List, page, 15, (int)articleList.Total);
            return View(articleList);
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(int? statusCode = null)
        {
            return View(statusCode);
        }

        [Route("/404.html")]
        public IActionResult NoFind()
        {
            return View();
        }
       
       
    }
}