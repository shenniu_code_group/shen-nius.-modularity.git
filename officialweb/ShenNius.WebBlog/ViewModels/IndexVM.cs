﻿using ShenNius.OfficialWeb.Core.Domain.Entity;
using ShenNius.OfficialWeb.Core.Dtos.Output;

namespace ShenNius.WebBlog.ViewModels
{
    public class IndexVM
    {
        public IndexVM(List<ArticleOutput> recArticleList, List<ArticleOutput> categoryArticleList, List<AdvList> friendlyLinkList)
        {
            RecArticleList = recArticleList != null ? recArticleList : new List<ArticleOutput>();
            CategoryArticleList = categoryArticleList != null ? categoryArticleList : new List<ArticleOutput>();
            FriendlyLinkList = friendlyLinkList != null ? friendlyLinkList : new List<AdvList>();
        }
       // public List<CmsColumnVM> AllChildColumn { get; private set; }
        public List<ArticleOutput> RecArticleList { get; private set; }
        public List<ArticleOutput> CategoryArticleList { get; private set; }
        public List<AdvList> FriendlyLinkList { get;private set; } 
    }
}
