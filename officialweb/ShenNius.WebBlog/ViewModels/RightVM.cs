﻿using ShenNius.OfficialWeb.Core.Dtos.Output;

namespace ShenNius.WebBlog.ViewModels
{
    public class RightVM
    {
        public RightVM(List<ArticleOutput> monthArticle, List<ArticleOutput> currentSpellArticle, List<string> allTags, bool isIndex)
        {
            MonthArticle=monthArticle!=null?monthArticle:new List<ArticleOutput>();
            CurrentSpellArticle=currentSpellArticle!=null?  currentSpellArticle:new List<ArticleOutput>();
            AllTags = allTags != null ? allTags : new List<string>();
            IsIndex= isIndex;    
        }
        public List<ArticleOutput> MonthArticle { get;private set; }
        public List<ArticleOutput> CurrentSpellArticle { get; private set; }
        public List<string> AllTags { get; private set; }
        public bool IsIndex { get; private set; }

    }
}
