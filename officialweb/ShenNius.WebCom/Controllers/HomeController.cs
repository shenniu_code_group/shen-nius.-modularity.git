﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ShenNius.OfficialWeb.Core.Common;
using ShenNius.OfficialWeb.Core.Domain.Entity;
using ShenNius.OfficialWeb.Core.Domain.Enum;
using ShenNius.OfficialWeb.Core.Domain.Services;
using ShenNius.OfficialWeb.Core.Dtos.Output;
using ShenNius.Repository;
using ShenNius.WebCom.Models;
using System.Diagnostics;
using X.PagedList;

namespace ShenNius.WebCom.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly CmsDomainService _cmsDomainService;
        private readonly IBaseRepository<AdvList> _advlistRepository;
        private readonly DomainConfig _domainConfig;
        private readonly int siteId = 1;
        public HomeController(IOptionsMonitor<DomainConfig> domainConfig, ILogger<HomeController> logger, CmsDomainService cmsDomainService, IBaseRepository<AdvList> advlistRepository)
        {
            _domainConfig = domainConfig.CurrentValue;
            _logger = logger;
            _cmsDomainService = cmsDomainService;
            _advlistRepository = advlistRepository;
            if (_domainConfig.SiteId > 0)
            {
                siteId = _domainConfig.SiteId;
            }
        }

        [HttpGet("/")]
        [Route("/index.html")]
        public IActionResult Index()
        {
            //var recArticleList = await _cmsDomainService.GetArtcileByConditionAsync((ca, cc) => ca.IsTop == true && ca.Audit == true && ca.TenantId == _domainConfig.SiteId, 1, 6);
            //var categoryArticleList = await _cmsDomainService.GetArtcileByConditionAsync((ca, cc) => ca.Audit == true && ca.TenantId == siteId, 1, 12);

            //var advList = await _advlistRepository.GetListAsync(x => x.IsDeleted == true && x.TenantId == siteId && x.Type == AdvEnum.FriendlyLink, x => x.Sort, false);
            //IndexVM vM = new IndexVM(recArticleList.Items, categoryArticleList.Items, advList);
            return View();
        }
        [Route("/solutions.html")]
        public IActionResult Solutions()
        {            
            return View();
        }
        [Route("/recruit.html")]
        public IActionResult Recruit()
        {
            return View();
        }
        
        [HttpGet]
        public IActionResult Detail()
        {           
            return View();
        }

        [HttpGet]
        [Route("/news.html")]
        public IActionResult NewsList()
        {
            
            return View();
        }

        [Route("/about-us.html")]
        public IActionResult AboutUs()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }
        [Route("/customer-case.html")]
        public IActionResult CustomerCase()
        {           
            return View();
        }
        [Route("/service-center.html")]
        public IActionResult ServiceCenter()
        {
          

            return View();
        }
      
        [HttpGet("/product-show.html")]
        public IActionResult ProductShow()
        {
           
            return View();
        }
       

        [Route("/404.html")]
        public IActionResult NoFind()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}