﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using ShenNius.OfficialWeb.Core.Common;
using ShenNius.OfficialWeb.Core.Domain.Services;

namespace ShenNius.WebCom.ViewComponents
{
    public class HeaderNavViewComponent : ViewComponent
    {
        private readonly DomainConfig _domainConfig;
        private readonly CmsDomainService _cmsDomainService;

        public HeaderNavViewComponent(IOptionsMonitor<DomainConfig> options, CmsDomainService cmsDomainService)
        {
            _domainConfig = options.CurrentValue;
            _cmsDomainService = cmsDomainService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var list = await _cmsDomainService.GetColumnAsync(_domainConfig.SiteId);           
            return View(list);
        }
    }
}
